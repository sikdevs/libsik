workspace "SIK"
  configurations {"Debug", "Release"}
  --platforms {"Static", "DLL"}
  includedirs {
    "include",
    "contrib/eigen",
  }
  warnings "Extra"
  --objdir "build/obj/%{cfg.platform}_%{cfg.buildcfg}"
  objdir "build/obj/%{cfg.buildcfg}"

  flags {"C++11"}

project "SIKlib"
  language "C++"
  files {
    "include/**.h",
    "src/**.cpp",
    "src/**.h"
  }
  --targetdir "build/lib/${cfg.platform}_%{cfg.buildcfg}"
  targetdir "build/lib/%{cfg.buildcfg}"

  --filter "platforms:Static"
    kind "StaticLib"

  --[[filter "platforms:DLL"
    kind "SharedLib"
    defines {"DLL_EXPORTS"}--]]

  filter "configurations:Debug"
    defines {"DEBUG"}
    symbols "On"
    debuglevel(2)

  filter "configurations:Release"
    defines {"NDEBUG"}
    optimize "On"

  -- reset filter for other settings
  filter{}
  
  -- openmp support
  configuration { "gmake", "-std=c++11" }
    buildoptions { "-fopenmp" }
    links { "gomp" }

  configuration { "vs*" }
    buildoptions { "/openmp" }   

project "Tests"
  kind "ConsoleApp"
  language "C++"
  --targetdir "build/tests/${cfg.platform}_%{cfg.buildcfg}"
  targetdir "build/tests/%{cfg.buildcfg}"
  includedirs { "tests/", "contrib/catch" }
  files {"tests/**.h", "tests/**.cpp"}
  links {"SIKlib"}

  filter "configurations:Debug"
    defines {"DEBUG"}
    symbols "On"
    debuglevel(2)

  filter "configurations:Release"
    defines {"NDEBUG"}
    optimize "On"
