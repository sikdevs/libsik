#include <catch.hpp>
#include <util.h>
using namespace SIK;

#include <Eigen/Dense>
using namespace Eigen;

#include <vector>
using namespace std;

TEST_CASE("Distance calculations", "[Util/Distance]")
{
    ArrayXd zero = ArrayXd::Zero(3);
    ArrayXd ones = ArrayXd::Ones(3);
    ArrayXd twos = ArrayXd::Constant(3, 2.0);

    SECTION("Euclidean distance") {
        SECTION("Distance to itself is 0") {
            REQUIRE(Util::Distance::euclidean(zero, zero) == 0);
            REQUIRE(Util::Distance::euclidean(ones, ones) == 0);
            REQUIRE(Util::Distance::euclidean(twos, twos) == 0);
        }

        SECTION("Distance is correct and symmetric") {
            REQUIRE(Util::Distance::euclidean(zero, ones) == sqrt(3));
            REQUIRE(Util::Distance::euclidean(ones, zero) == sqrt(3));
            REQUIRE(Util::Distance::euclidean(zero, twos) == sqrt(12));
            REQUIRE(Util::Distance::euclidean(twos, zero) == sqrt(12));
            REQUIRE(Util::Distance::euclidean(ones, twos) == sqrt(3));
            REQUIRE(Util::Distance::euclidean(twos, ones) == sqrt(3));
        }
    }

    SECTION("Squared Euclidean distance") {
        SECTION("Distance to itself is 0") {
            REQUIRE(Util::Distance::sqeuclidean(zero, zero) == 0);
            REQUIRE(Util::Distance::sqeuclidean(ones, ones) == 0);
            REQUIRE(Util::Distance::sqeuclidean(twos, twos) == 0);
        }

        SECTION("Distance is correct and symmetric") {
            REQUIRE(Util::Distance::sqeuclidean(zero, ones) == 3);
            REQUIRE(Util::Distance::sqeuclidean(ones, zero) == 3);
            REQUIRE(Util::Distance::sqeuclidean(zero, twos) == 12);
            REQUIRE(Util::Distance::sqeuclidean(twos, zero) == 12);
            REQUIRE(Util::Distance::sqeuclidean(ones, twos) == 3);
            REQUIRE(Util::Distance::sqeuclidean(twos, ones) == 3);
        }
    }

    SECTION("Find nearest vector from a list") {
        vector<VectorXd> centers{ zero, ones, twos };
        REQUIRE(Util::nearest(centers, zero) == 0);
        REQUIRE(Util::nearest(centers, ones) == 1);
        REQUIRE(Util::nearest(centers, twos) == 2);
        REQUIRE(Util::nearest(centers, -ones) == 0);
    }
}