#include <catch.hpp>
#include <trees/decisionTree.h>
#include <vector>
#include <iostream>
#include <Eigen/Dense>
#include <util.h>
using namespace SIK;
using namespace Eigen;
using namespace std;

TEST_CASE("Tree functions", "[trees/DecisionTree]")
{
    /*
    ArrayXXd X_iris = ArrayXXd(150, 4);
    ArrayXd y_iris = ArrayXd(150);
    X_iris << 5.1, 3.5, 1.4, 0.2, 4.9, 3.0, 1.4, 0.2, 4.7, 3.2, 1.3, 0.2, 4.6, 3.1, 1.5,
           0.2, 5.0, 3.6, 1.4, 0.2, 5.4, 3.9, 1.7, 0.4, 4.6, 3.4, 1.4, 0.3, 5.0, 3.4, 1.5, 0.2,
           4.4, 2.9, 1.4, 0.2, 4.9, 3.1, 1.5, 0.1, 5.4, 3.7, 1.5, 0.2, 4.8, 3.4, 1.6, 0.2, 4.8,
           3.0, 1.4, 0.1, 4.3, 3.0, 1.1, 0.1, 5.8, 4.0, 1.2, 0.2, 5.7, 4.4, 1.5, 0.4, 5.4, 3.9,
           1.3, 0.4, 5.1, 3.5, 1.4, 0.3, 5.7, 3.8, 1.7, 0.3, 5.1, 3.8, 1.5, 0.3, 5.4, 3.4, 1.7,
           0.2, 5.1, 3.7, 1.5, 0.4, 4.6, 3.6, 1.0, 0.2, 5.1, 3.3, 1.7, 0.5, 4.8, 3.4, 1.9, 0.2,
           5.0, 3.0, 1.6, 0.2, 5.0, 3.4, 1.6, 0.4, 5.2, 3.5, 1.5, 0.2, 5.2, 3.4, 1.4, 0.2, 4.7,
           3.2, 1.6, 0.2, 4.8, 3.1, 1.6, 0.2, 5.4, 3.4, 1.5, 0.4, 5.2, 4.1, 1.5, 0.1, 5.5, 4.2,
           1.4, 0.2, 4.9, 3.1, 1.5, 0.1, 5.0, 3.2, 1.2, 0.2, 5.5, 3.5, 1.3, 0.2, 4.9, 3.1, 1.5,
           0.1, 4.4, 3.0, 1.3, 0.2, 5.1, 3.4, 1.5, 0.2, 5.0, 3.5, 1.3, 0.3, 4.5, 2.3, 1.3, 0.3,
           4.4, 3.2, 1.3, 0.2, 5.0, 3.5, 1.6, 0.6, 5.1, 3.8, 1.9, 0.4, 4.8, 3.0, 1.4, 0.3, 5.1,
           3.8, 1.6, 0.2, 4.6, 3.2, 1.4, 0.2, 5.3, 3.7, 1.5, 0.2, 5.0, 3.3, 1.4, 0.2, 7.0, 3.2,
           4.7, 1.4, 6.4, 3.2, 4.5, 1.5, 6.9, 3.1, 4.9, 1.5, 5.5, 2.3, 4.0, 1.3, 6.5, 2.8, 4.6,
           1.5, 5.7, 2.8, 4.5, 1.3, 6.3, 3.3, 4.7, 1.6, 4.9, 2.4, 3.3, 1.0, 6.6, 2.9, 4.6, 1.3,
           5.2, 2.7, 3.9, 1.4, 5.0, 2.0, 3.5, 1.0, 5.9, 3.0, 4.2, 1.5, 6.0, 2.2, 4.0, 1.0, 6.1,
           2.9, 4.7, 1.4, 5.6, 2.9, 3.6, 1.3, 6.7, 3.1, 4.4, 1.4, 5.6, 3.0, 4.5, 1.5, 5.8, 2.7,
           4.1, 1.0, 6.2, 2.2, 4.5, 1.5, 5.6, 2.5, 3.9, 1.1, 5.9, 3.2, 4.8, 1.8, 6.1, 2.8, 4.0,
           1.3, 6.3, 2.5, 4.9, 1.5, 6.1, 2.8, 4.7, 1.2, 6.4, 2.9, 4.3, 1.3, 6.6, 3.0, 4.4, 1.4,
           6.8, 2.8, 4.8, 1.4, 6.7, 3.0, 5.0, 1.7, 6.0, 2.9, 4.5, 1.5, 5.7, 2.6, 3.5, 1.0, 5.5,
           2.4, 3.8, 1.1, 5.5, 2.4, 3.7, 1.0, 5.8, 2.7, 3.9, 1.2, 6.0, 2.7, 5.1, 1.6, 5.4, 3.0,
           4.5, 1.5, 6.0, 3.4, 4.5, 1.6, 6.7, 3.1, 4.7, 1.5, 6.3, 2.3, 4.4, 1.3, 5.6, 3.0, 4.1,
           1.3, 5.5, 2.5, 4.0, 1.3, 5.5, 2.6, 4.4, 1.2, 6.1, 3.0, 4.6, 1.4, 5.8, 2.6, 4.0, 1.2,
           5.0, 2.3, 3.3, 1.0, 5.6, 2.7, 4.2, 1.3, 5.7, 3.0, 4.2, 1.2, 5.7, 2.9, 4.2, 1.3, 6.2,
           2.9, 4.3, 1.3, 5.1, 2.5, 3.0, 1.1, 5.7, 2.8, 4.1, 1.3, 6.3, 3.3, 6.0, 2.5, 5.8, 2.7,
           5.1, 1.9, 7.1, 3.0, 5.9, 2.1, 6.3, 2.9, 5.6, 1.8, 6.5, 3.0, 5.8, 2.2, 7.6, 3.0, 6.6,
           2.1, 4.9, 2.5, 4.5, 1.7, 7.3, 2.9, 6.3, 1.8, 6.7, 2.5, 5.8, 1.8, 7.2, 3.6, 6.1, 2.5,
           6.5, 3.2, 5.1, 2.0, 6.4, 2.7, 5.3, 1.9, 6.8, 3.0, 5.5, 2.1, 5.7, 2.5, 5.0, 2.0, 5.8,
           2.8, 5.1, 2.4, 6.4, 3.2, 5.3, 2.3, 6.5, 3.0, 5.5, 1.8, 7.7, 3.8, 6.7, 2.2, 7.7, 2.6,
           6.9, 2.3, 6.0, 2.2, 5.0, 1.5, 6.9, 3.2, 5.7, 2.3, 5.6, 2.8, 4.9, 2.0, 7.7, 2.8, 6.7,
           2.0, 6.3, 2.7, 4.9, 1.8, 6.7, 3.3, 5.7, 2.1, 7.2, 3.2, 6.0, 1.8, 6.2, 2.8, 4.8, 1.8,
           6.1, 3.0, 4.9, 1.8, 6.4, 2.8, 5.6, 2.1, 7.2, 3.0, 5.8, 1.6, 7.4, 2.8, 6.1, 1.9, 7.9,
           3.8, 6.4, 2.0, 6.4, 2.8, 5.6, 2.2, 6.3, 2.8, 5.1, 1.5, 6.1, 2.6, 5.6, 1.4, 7.7, 3.0,
           6.1, 2.3, 6.3, 3.4, 5.6, 2.4, 6.4, 3.1, 5.5, 1.8, 6.0, 3.0, 4.8, 1.8, 6.9, 3.1, 5.4,
           2.1, 6.7, 3.1, 5.6, 2.4, 6.9, 3.1, 5.1, 2.3, 5.8, 2.7, 5.1, 1.9, 6.8, 3.2, 5.9, 2.3,
           6.7, 3.3, 5.7, 2.5, 6.7, 3.0, 5.2, 2.3, 6.3, 2.5, 5.0, 1.9, 6.5, 3.0, 5.2, 2.0, 6.2,
           3.4, 5.4, 2.3, 5.9, 3.0, 5.1, 1.8;
    y_iris << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1,
           1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
           1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2,
           2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
           2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2;
    Trees::DecisionTree<Trees::BasicSplit> dt(25, 20);
    SECTION("iris_data normal") {
        dt.fit(X_iris, y_iris);
        double n_correct = 0;

        for (size_t d = 0; d < size_t(y_iris.rows()); ++d) {
            Eigen::ArrayXd row = X_iris.row(d);

            if (dt.predict(row) == y_iris(d)) {
                n_correct += 1;
            }
        }

        REQUIRE(n_correct / double(y_iris.rows()) >= 0.9);
        Trees::DecisionTree dtD(25);
        dtD.deserialize(dt.serialize());
        REQUIRE(dt.serialize() == dtD.serialize());
    }
    SECTION("iris_data reject") {
        SECTION("MinMax") {
            double n_rejected = 0;

            for (int c = 0; c < 3; ++c) {
                std::vector<int> indexes[2];
                Util::EigenTools::indexFilter(y_iris, [c](double data) {
                    return data != c;
                }, indexes);
                Trees::DecisionTree tree(25, Trees::DecisionTree::MINMAX_REJECTION);
                Eigen::ArrayXXd X_train = Util::EigenTools::indexedRowAccess(X_iris,
                                          indexes[0].begin(), indexes[0].end());
                Eigen::ArrayXd y_train = Util::EigenTools::indexedRowAccess(y_iris,
                                         indexes[0].begin(), indexes[0].end());
                Eigen::ArrayXXd X_test  = Util::EigenTools::indexedRowAccess(X_iris,
                                          indexes[1].begin(), indexes[1].end());
                Eigen::ArrayXd y_test  = Util::EigenTools::indexedRowAccess(y_iris,
                                         indexes[1].begin(), indexes[1].end());
                tree.fit(X_train, y_train);

                for (size_t t = 0; t < size_t(y_test.rows()); ++t) {
                    Eigen::ArrayXd row = X_test.row(t);

                    if (tree.predict(row) == tree.classes.size()) {
                        n_rejected++;
                    }
                }

                REQUIRE(n_rejected / double(y_iris.rows()) >= 0.3);
            }
        }
        SECTION("MeanStd Rejection") {
            double n_rejected = 0;

            for (int c = 0; c < 3; ++c) {
                std::vector<int> indexes[2];
                Util::EigenTools::indexFilter(y_iris, [c](double data) {
                    return data != c;
                }, indexes);
                Trees::DecisionTree tree(25, false, Trees::DecisionTree::MEANSTD_REJECTION, 2.5);
                Eigen::ArrayXXd X_train = Util::EigenTools::indexedRowAccess(X_iris,
                                          indexes[0].begin(), indexes[0].end());
                Eigen::ArrayXd  y_train = Util::EigenTools::indexedRowAccess(y_iris,
                                          indexes[0].begin(), indexes[0].end());
                Eigen::ArrayXXd X_test  = Util::EigenTools::indexedRowAccess(X_iris,
                                          indexes[1].begin(), indexes[1].end());
                Eigen::ArrayXd  y_test  = Util::EigenTools::indexedRowAccess(y_iris,
                                          indexes[1].begin(), indexes[1].end());
                tree.fit(X_train, y_train);

                for (size_t t = 0; t < size_t(y_test.rows()); ++t) {
                    Eigen::ArrayXd row = X_test.row(t);

                    if (tree.predict(row) == tree.classes.size()) {
                        n_rejected++;
                    }
                }
            }

            REQUIRE(n_rejected / double(y_iris.rows()) >= 0.3);
        }
        SECTION("Mahalanobis Rejection") {
            double n_rejected = 0;

            for (int c = 0; c < 3; ++c) {
                std::vector<int> indexes[2];
                Util::EigenTools::indexFilter(y_iris, [c](double data) {
                    return data != c;
                }, indexes);
                Trees::DecisionTree tree(25, Trees::DecisionTree::MAHALANOBIS_REJECTION);
                Eigen::ArrayXXd X_train = Util::EigenTools::indexedRowAccess(X_iris,
                                          indexes[0].begin(), indexes[0].end());
                Eigen::ArrayXd  y_train = Util::EigenTools::indexedRowAccess(y_iris,
                                          indexes[0].begin(), indexes[0].end());
                Eigen::ArrayXXd X_test  = Util::EigenTools::indexedRowAccess(X_iris,
                                          indexes[1].begin(), indexes[1].end());
                Eigen::ArrayXd  y_test  = Util::EigenTools::indexedRowAccess(y_iris,
                                          indexes[1].begin(), indexes[1].end());
                tree.fit(X_train, y_train);
                tree.mahal_sigma = 0.2;

                for (size_t t = 0; t < size_t(y_test.rows()); ++t) {
                    Eigen::ArrayXd row = X_test.row(t);

                    if (tree.predict(row) == tree.classes.size()) {
                        n_rejected++;
                    }
                }
            }

            REQUIRE(n_rejected / double(y_iris.rows()) >= 0.3);
        }
    }
    */
}
