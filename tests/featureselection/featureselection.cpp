#include <catch.hpp>
#include <featureselection/featureselect.h>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <map>

using namespace SIK;
using namespace Eigen;
using namespace std;
TEST_CASE("FEATURESELECTION", "[featureselection/bov]")
{

    SECTION("parseData") {
        auto getData = []() {
            std::ifstream input("meta/features-lego.csv");
            //see http://stackoverflow.com/a/7868998
            auto getNextLineAndSplitIntoTokens = [](std::istream & str) {
                std::vector<std::string>   result;
                std::string                line;
                std::getline(str, line);

                std::stringstream          lineStream(line);
                std::string                cell;

                while (std::getline(lineStream, cell, ',')) {
                    result.push_back(cell);
                }

                // This checks for a trailing comma with no data after it.
                if (!lineStream && cell.empty()) {
                    // If there was a trailing comma then add an empty element.
                    result.push_back("");
                }

                return result;
            };
            int rows = 0, cols = 0;
            std::string l;

            //see http://stackoverflow.com/a/3482093
            while (std::getline(input, l)) {
                ++rows;
            }

            input.clear();
            input.seekg(0, ios::beg);

            std::vector<std::string> line = getNextLineAndSplitIntoTokens(input);
            cols = line.size();

            REQUIRE(rows > 1);
            REQUIRE(cols > 0);
            int r = 0;
            Eigen::ArrayXXd data(rows - 1, cols);
            std::map<string, int> classes;

            while (r < rows - 1) {
                line = getNextLineAndSplitIntoTokens(input);

                if (classes.find(line[0]) == classes.end()) {
                    classes[line[0]] = classes.size();
                }

                data(r, 0) = classes[line[0]];

                for (int c = 1; c < cols; ++c) {
                    try {
                        data(r, c) = std::stod(line[c]);
                    } catch (const std::exception &) {
                        std::cout << "parsing error" << endl;
                    }
                }

                ++r;
            }

            return data;
        };
        ArrayXXd data = getData();
        ArrayXXd X_mixed = data.block(0, 2, data.rows(), data.cols() - 2);

        ArrayXi y = data.block(0, 0, data.rows(), 1).cast<int>();
        ArrayXXd X = ArrayXXd::Map(X_mixed.data(), X_mixed.rows(), X_mixed.cols() / 2,
                                   OuterStride<>(2 * X_mixed.rows()));
        ArrayXXd C = ArrayXXd::Map(X_mixed.data() + X_mixed.rows(), X_mixed.rows(),
                                   X_mixed.cols() / 2, OuterStride<>(2 * X_mixed.rows()));


        auto sel = SIK::Featureselection::CostSensitive::select(X, y, C, 10, 100);

        REQUIRE(sel.size() == 10);

        cout << std::setw(10) << "Merit" << std::setw(15) << "Mean cost" << std::setw(
                 15) << "Cost scatter" << endl;

        for (const auto &s : sel) {
            cout << std::setw(10) << s.merit << std::setw(15) << s.meanCost << std::setw(
                     15) << s.costScatter << endl;
        }
    }
}
