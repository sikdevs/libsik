#include <catch.hpp>
#include <json/json.h>
#include <vector>
#include <iostream>
#include <Eigen/Dense>
#include <util.h>
using namespace SIK;
using namespace Eigen;
using namespace std;
TEST_CASE("Json", "[json/json]")
{
    SECTION("Basics") {
        std::shared_ptr<Json::Object> object(new Json::Object());
        REQUIRE(object->str() == "{}");
        object->put("bool_true", true);
        object->put("bool_false", false);
        object->put("number", 42.0);
        object->put("negNumber", -3.14);
        object->put("largeNumber", 10.0e5);
        object->put("null_value");
        object->put("string", std::string("lorem ipsum"));

        REQUIRE(object->str() ==
                "{\"bool_false\":false,\"bool_true\":true,\"largeNumber\":1000000.000000,\"negNumber\":-3.140000,\"null_value\":null,\"number\":42.000000,\"string\":\"lorem ipsum\"}");
    }
    SECTION("Arrays") {
        Eigen::ArrayXd emptyVector(0);
        auto emptyArr = make_shared<Json::Array>(emptyVector);
        REQUIRE(emptyArr->str() == "[]");

        Eigen::ArrayXd scalar(1);
        scalar << 1.0;
        auto scalarArr = make_shared<Json::Array>(scalar);
        std::cout << scalarArr->eigenArrayXXd() << std::endl;
        REQUIRE(scalarArr->str() == "[[1.000000]]");

        std::shared_ptr<Json::Array> array(new Json::Array());
        array->push_back(std::string("foo"));
        REQUIRE(array->str() == "[\"foo\"]");
        Eigen::ArrayXXd e(3, 3);
        e.col(0) = Eigen::ArrayXd::LinSpaced(3, 0, 3);
        e.col(1) = Eigen::ArrayXd::LinSpaced(3, 0, 2);
        e.col(2) = Eigen::ArrayXd::LinSpaced(3, 0, 1);
        std::shared_ptr<Json::Array> arr(new Json::Array(e));
        array->push_back(arr);
        REQUIRE(array->str() ==
                "[\"foo\",[[0.000000,0.000000,0.000000],[1.500000,1.000000,0.500000],[3.000000,2.000000,1.000000]]]");
        array->push_back(false);
        REQUIRE(array->str() ==
                "[\"foo\",[[0.000000,0.000000,0.000000],[1.500000,1.000000,0.500000],[3.000000,2.000000,1.000000]],false]");
        array->push_back(1.0);
        REQUIRE(array->str() ==
                "[\"foo\",[[0.000000,0.000000,0.000000],[1.500000,1.000000,0.500000],[3.000000,2.000000,1.000000]],false,1.000000]");

        std::stringstream ss;
        ss << arr->str();
        auto eig = Json::Array::parse(ss);
        Eigen::ArrayXXd e2 = eig->eigenArrayXXd();

        for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
                REQUIRE(e2(i, j) == e(i, j));
            }
        }
    }
    SECTION("Complex Tests") {
        std::shared_ptr<Json::Object> i(new Json::Object());
        i->put("number#1", 3.0);
        i->put("number#2", 1.5);
        i->put("string", std::string("wololo"));
        i->put("boolean", false);
        Eigen::ArrayXXd eig(3, 4);
        eig << 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12;
        std::shared_ptr<Json::Array> t(new Json::Array(eig));
        //i.put("eigen", &t);
        t->push_back(1.0);
        t->push_back(std::string("hehe"));
        t->push_back(i);

        stringstream ss;
        ss << t->str();
        auto test = Json::Array::parse(ss);
        REQUIRE(test->str() == t->str());
    }
}
