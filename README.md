# SIK - Smart Inspection Kit

Machine learning tools for automated visual inspection.

## Building

* Clone repository
* Obtain [premake5](http://premake.github.io/download.html)
* Generate build files (see [here](https://github.com/premake/premake-core/wiki/Using-Premake), e.g., `premake5 vs2013` for Visual Studio 2013 or `premake gmake` for GNU Make.
* Open Project files / run `make` / ...

## Third party libraries

Third party libraries are located in the `contrib` directory.

### [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) 3.2.9

Matrices, linear algebra, numerical solvers, ...

Licensed under [MPL2](https://www.mozilla.org/en-US/MPL/2.0/).

### [Catch](https://github.com/philsquared/Catch)

Automated test framework for C++ in a single header file.

Licensed under the Boost Software License -- see `contrib/catch/LICENSE_1_0.txt`.
