#include <optimize/NSGA2.h>
#include <sstream>
#include <iostream>
#include <limits>
#include <map>
#include <algorithm>
#include <random>
#include <omp.h>

using namespace Eigen;
using namespace std;

namespace SIK
{
namespace Optimize
{

namespace // hidden
{
bool dominates(const NSGA2::Individual &a, const NSGA2::Individual &b)
{
    // all(a.fitness .< b.fitness)
    for (size_t i = 0; i < a.fitness.size(); ++i) {
        if (a.fitness[i] >= b.fitness[i]) {
            return false;
        }
    }

    return true;
}

random_device rd;
mt19937 rng(rd());
}

NSGA2 &NSGA2::crowdingDistanceAssignment(vector<NSGA2::Individual *> &individuals)
{
    auto N = individuals.size();

    if (N <= 0) {
        return *this;
    }

    for (auto *p : individuals) {
        p->distance = 0;
    }

    for (size_t m = 0; m < objectiveCount; m++) {
        sort(individuals.begin(), individuals.end(), [m](const NSGA2::Individual * a,
        const NSGA2::Individual * b) {
            return a->fitness[m] < b->fitness[m];
        });

        individuals[0]->distance = individuals[N - 1]->distance =
                                       numeric_limits<double>::infinity();

        for (size_t i = 1; i < N - 1; i++) {
            auto *left = individuals[i - 1];
            auto *right = individuals[i + 1];
            individuals[i]->distance += (right->fitness[m] - left->fitness[m]) / objectiveRange(
                                            m);
        }
    }

    return *this;
}


vector<vector<NSGA2::Individual *>> NSGA2::fastNonDominantedSort()
{
    map<NSGA2::Individual *, int> domination_counter; //n_p
    map<NSGA2::Individual *, vector<NSGA2::Individual *>> dominated_by; //S_p
    vector<vector<NSGA2::Individual *>> fronts; //F_i
    fronts.push_back(vector<NSGA2::Individual *>());

    // compute domination count and get pareto front
    for (auto &p : population) {
        for (auto &q : population) {
            if (&p == &q) {
                // nothing
            } else if (dominates(p, q)) {
                dominated_by[&p].push_back(&q);
            } else if (dominates(q, p)) {
                domination_counter[&p] += 1;
            }
        }

        if (domination_counter[&p] == 0) {
            p.rank = 0;
            fronts[0].push_back(&p);
        }
    }

    // sort remaining individuals into pareto shells
    size_t i = 0;

    while (!fronts[i].empty()) {
        vector<NSGA2::Individual *> Q;

        for (auto &p : fronts[i]) {
            for (auto &q : dominated_by[p]) {
                domination_counter[q] -= 1;

                if (domination_counter[q] == 0) {
                    q->rank = i + 1;
                    Q.push_back(q);
                }
            }
        }

        i += 1;
        fronts.push_back(Q);
    }

    // last front is empty - remove it
    fronts.pop_back();

    return fronts;
}

NSGA2::Individual NSGA2::tournamentSelection(vector<Individual> &parents)
{
    uniform_int_distribution<int> uni(0, parents.size() - 1);
    auto p1 = uni(rng);
    auto p2 = uni(rng);
    return parents[p1] < parents[p2] ? parents[p1] : parents[p2];
}

NSGA2 &NSGA2::generateOffspring()
{
    vector<NSGA2::Individual> offspring;
    offspring.resize(populationSize);

    #pragma omp parallel for

    for (int i = 0; i < int(populationSize); ++i) {
        auto p1 = NSGA2::tournamentSelection(population);
        auto p2 = NSGA2::tournamentSelection(population);
        offspring[i] = mutate(recombine(p1, p2));
        assign_fitness(offspring[i]);
    }

    population.insert(population.end(), offspring.begin(), offspring.end());
    return *this;
}

NSGA2 &NSGA2::optimize(Eigen::ArrayXXd &bounds_objective)
{
    objectiveCount = bounds_objective.rows();
    objectiveRange = bounds_objective.col(1) - bounds_objective.col(0);
    generation = 0;
    population.resize(populationSize);

    #pragma omp parallel for

    for (int i = 0; i < (populationSize); ++i) {
        population[i] = generator();
    }

    auto F = fastNonDominantedSort();

    for (size_t i = 0; i < F.size(); ++i) {
        crowdingDistanceAssignment(F[i]);
    }

    for (size_t g = 0; g < maxGeneration; ++g) {
        generateOffspring();

        auto F = fastNonDominantedSort();
        size_t i = 0;
        vector<NSGA2::Individual> pop_new;
        pop_new.reserve(populationSize);

        while (pop_new.size() + F[i].size() < populationSize) {
            crowdingDistanceAssignment(F[i]);

            for (auto *ind : F[i]) {
                pop_new.push_back(*ind);
            }

            i += 1;
        }

        sort(F[i].begin(), F[i].end(), [](const NSGA2::Individual * a,
        const NSGA2::Individual * b) {
            return dominates(*a, *b);
        });

        for (auto *ind : F[i]) {
            if (pop_new.size() >= populationSize) {
                break;
            }

            pop_new.push_back(*ind);
        }

        swap(population, pop_new);
        generation++;
    }

    return *this;
}

}
}
