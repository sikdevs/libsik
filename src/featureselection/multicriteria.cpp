#include <featureselection/multicriteria.h>
#include <util.h>
#include <sstream>
#include <iostream>
#include <limits>
#include <map>
#include <algorithm>
#include <random>
#include <Eigen/SVD>

using namespace Eigen;
using namespace std;

namespace
{
double corr(const ArrayXd &x, const ArrayXd &y)
{
    ArrayXd s = x - x.mean();  // s, t = x - mean(x), y - mean(y)
    ArrayXd t = y - y.mean();

    double d = s.matrix().norm() *
               t.matrix().norm();  // d = nrm2(length(s),s,1) * nrm2(length(t),t,1)
    return d != 0 ? s.matrix().dot(t.matrix()) / d : 0;
}

inline ArrayXXd xTx(const ArrayXXd &x)
{
    return (x.transpose().matrix() * x.matrix()).array();
}; //check if this works properly
}

namespace SIK
{
namespace Featureselection
{

double Multicriteria::multicorr(const ArrayXXd &X, const ArrayXi &y)
{
    MatrixXd c(X.cols(), 1);  // c, R = zeros(size(X,2)), eye(size(X,2), size(X,2))
    MatrixXd R = MatrixXd::Identity(X.cols(), X.cols());

    // for i = 1:size(X,2)
    //   @inbounds c[i] = corr(X[:,i], y)
    //   @simd for k = (i+1):size(X,2)
    //     @inbounds R[k,i] = R[i,k] = corr(X[:,i], X[:,k])
    //   end
    // end
    auto yd = y.cast<double>();

    for (auto i = 0; i < X.cols(); ++i) {
        c(i) = corr(X.col(i), yd);

        for (auto k = i + 1; k < X.cols(); ++k) {
            R(k, i) = corr(X.col(i), X.col(k));
            R(i, k) = R(k, i);
        }
    }

    // #sqrt(c' * inv(R) * c)[1]
    // BLAS.gemv('t', reshape(BLAS.gemv('t', inv(R), c), (length(c), 1)), c)[1] |> sqrt # = (iR'c)'c = (c'iR'')c = c'iR c
    return sqrt((c.transpose() * R.inverse() * c)(0));
}


double Multicriteria::LDA_score(const ArrayXXd &X, const ArrayXi &y)
{
    // counts the number of samples per class
    auto Nsamp = map<int, int>();

    for (int i = 0; i < y.size(); ++i) {
        Nsamp[y(i)]++;
    }

    // get class labels and map from label to id
    auto classes = vector<int>();
    classes.reserve(Nsamp.size());
    auto class_index = map<int, int>();

    for (auto const &it : Nsamp) {
        classes.push_back(it.first);
        class_index[it.first] = class_index.size();
    }

    Eigen::VectorXd sampleMean = VectorXd::Zero(X.cols());
    Eigen::MatrixXd classMeans = MatrixXd::Zero(classes.size(), X.cols());
    MatrixXd Sw = MatrixXd::Zero(X.cols(), X.cols()); // (total) scatter within classes
    MatrixXd Sb = MatrixXd::Zero(X.cols(), X.cols()); // scatter between classes
    int N = X.rows();

    // compute class-wise and total mean
    for (int k = 0; k < N; ++k) {
        auto c = y[k];
        VectorXd x = X.row(k);
        classMeans.row(class_index[c]) += x / Nsamp[c];
        sampleMean += x / N;
    }

    // compute scatter within classes
    for (int k = 0; k < N; ++k) {
        auto c = y[k];
        auto m = classMeans.row(class_index[c]);
        VectorXd d = X.row(k).matrix() - m;
        Sw += d * d.transpose();
    }

    // compute scatter between classes
    for (auto c : classes) {
        VectorXd d = classMeans.row(class_index[c]).transpose() - sampleMean;
        Sb += Nsamp[c] * d * d.transpose();
    }


    // solve objective for weight matrix and return value of objective
    // note: objective   J(W) = Tr{ (W Sw W^T)^-1  (W Sb W^T) }
    //       optimal W is solution to eigenvector problem: (Sw^-1 Sb) v = λv
    // see C.M. Bishop: Pattern Recognition and Machine Learning, p 191-192
    EigenSolver<MatrixXd> eigensolver(Util::EigenTools::pinv(Sw)*Sb);
    Eigen::MatrixXd W = (eigensolver.eigenvectors().leftCols(classes.size())).real();
    Eigen::MatrixXd M1 = W.transpose() * Sw * W;
    Eigen::MatrixXd M2 = W.transpose() * Sb * W;

    return (Util::EigenTools::pinv(M1) * M2).trace();
}
}
}
