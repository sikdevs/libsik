#include <featureselection/featureselect.h>
#include <featureselection/multicriteria.h>
#include <optimize/NSGA2.h>
#include <sstream>
#include <limits>
#include <map>
#include <vector>
#include <algorithm>
#include <iostream>
#include <random>

using namespace Eigen;
using namespace std;

namespace
{
random_device rd;
mt19937 rng(rd());
bernoulli_distribution coinFlip(.5);
}

namespace SIK
{
namespace Featureselection
{
vector<CostSensitive::Selection> CostSensitive::select(ArrayXXd &X, ArrayXi &y,
        ArrayXXd &C, size_t numCandidates, size_t iterations)
{
    typedef SIK::Optimize::NSGA2::Individual Individual;

    // evaluate objectives of a solution
    std::function<Individual(Individual&)> assign_fitness = [&C, &X, &y](Individual & individual) -> Individual {
        using Util::EigenTools::booledColAccess;
        auto &selection = individual.chromosome;
        auto features = booledColAccess(X, selection.begin(), selection.end());

        auto selcost = booledColAccess(C, selection.begin(), selection.end());
        auto meancost = selcost.rowwise().mean();
        auto costScatter = ((selcost.colwise() - meancost).pow(2).rowwise().sum() / (selcost.cols() - 1)).sqrt();

        individual.fitness.push_back(-Multicriteria::LDA_score(features, y));
        individual.fitness.push_back(meancost.sum());
        individual.fitness.push_back(costScatter.sum());
        return individual;
    };

    // generate random individual
    std::function<Individual()> random_individual = [&C, &X, &y, &assign_fitness]() -> Individual {
        Individual individual;

        for (auto i = 0; i < X.cols() - 1; ++i)
        {
            individual.chromosome.push_back(coinFlip(rng));
        }

        return assign_fitness(individual);
    };

    // recombine two solutions
    std::function<Individual(Individual, Individual)> recombine = [](Individual a, Individual b) -> Individual {
        Individual recombined;

        for (size_t i = 0; i < a.chromosome.size(); ++i)
        {
            // 50% chance to inherit from either parent
            recombined.chromosome.push_back(coinFlip(rng) ? a.chromosome[i] : b.chromosome[i]);
        }

        return recombined;
    };

    // flip bits with 1 / |chromosome| chance
    bernoulli_distribution randomMutation(1.0 / float(X.cols()));
    std::function<Individual(Individual)> mutate = [&randomMutation](Individual x) -> Individual {
        for (size_t i = 0; i < x.chromosome.size(); ++i)
        {
            x.chromosome[i] = randomMutation(rng) ? !x.chromosome[i] : x.chromosome[i];
        }

        return x;
    };

    ArrayXXd objective_bounds(3, 2);
    objective_bounds << -100, 0, // bounds for LDA coefficient (not really though)
                     0, C.colwise().mean().sum(), // bounds for mean cost
                     0, ((C.rowwise() - C.colwise().mean()).pow(2).colwise().sum() /
                         (C.rows() - 1)).sqrt().sum(); // bounds for cost scatter

    Optimize::NSGA2 optimizer = Optimize::NSGA2(numCandidates, iterations,
                                random_individual, assign_fitness,
                                recombine, mutate).optimize(objective_bounds);

    vector<Selection> ret;
    ret.reserve(optimizer.population.size());

    for (const auto &p : optimizer.population) {
        Selection s;

        for (size_t i = 0; i < p.chromosome.size(); ++i) {
            if (p.chromosome[i]) {
                s.featureIndices.push_back(i);
            }
        }

        s.merit = -p.fitness[0];
        s.meanCost = p.fitness[1];
        s.costScatter = p.fitness[2];

        ret.push_back(s);
    }

    return ret;
}
}
}
