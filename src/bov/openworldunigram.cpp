#include <bov/openworldunigram.h>
#include <util.h>
#include <sstream>
#include <limits>

using namespace Eigen;
using namespace std;

namespace SIK
{
namespace BOV
{

ArrayXd OpenWorldUnigram::_transformSoftAssignment(const ArrayXXd &X) const
{
    ArrayXd out = ArrayXd::Zero(gmm.K + 1);
    size_t nAccepted = 0, nRejected = 0;

    for (int i = X.rows() - 1; i >= 0; --i) {
        ArrayXd transform = gmm.transform(X.row(i));

        if (transform.sum() < theta) {
            nRejected++;
        } else {
            out += transform;
            nAccepted++;
        }
    }

    out *= 1.0 / double(nAccepted);
    out(0) = double(nRejected) / double(X.rows());
    return out;
}

ArrayXd OpenWorldUnigram::_transformHardAssignment(const ArrayXXd &X)const
{
    ArrayXd out = ArrayXd::Zero(gmm.K + 1);
    size_t nAccepted = 0, nRejected = 0;

    for (int i = X.rows() - 1; i >= 0; --i) {
        ArrayXd transform = gmm.transform(X.row(i));

        if (transform.sum() < theta) {
            nRejected++;
        } else {
            int k;
            transform.maxCoeff(&k);
            out(k + 1) += 1;
        }
    }

    out *= 1.0 / double(nAccepted);
    out(0) = double(nRejected) / double(X.rows());
    return out;
}

OpenWorldUnigram &OpenWorldUnigram::fit(const ArrayXXd &X, const double rho)
{
    gmm.fit(X);

    // find rejection threshold
    double upper = numeric_limits<double>::min(), lower = numeric_limits<double>::max();
    int n_rejectedRows = X.rows();

    for (size_t i = 0; i < size_t(X.rows()); ++i) {
        double sum = gmm.transform(X.row(i)).sum();
        upper = max(sum, upper);
        lower = min(sum, upper);
    }

    for (size_t i = 0; i < 50; ++i) {
        //binary search
        theta = lower + ((upper - lower) / 2);
        n_rejectedRows = 0;

        for (size_t i = 0; i < size_t(X.rows()); ++i) {
            if (gmm.transform(X.row(i)).sum() < theta) {
                n_rejectedRows += 1;
            }
        }

        if (n_rejectedRows / X.rows() > rho) {
            upper = theta;
        } else if (n_rejectedRows / X.rows() < rho) {
            lower = theta;
        } else {
            break;
        }
    }

    theta = lower + ((upper - lower) / 2);
    return *this;
}



std::shared_ptr<Json::Object> OpenWorldUnigram::toJson()const
{
    std::shared_ptr<Json::Object> obj = std::make_shared<Json::Object>();
    obj->put("type", std::string("OpenWorldUnigram"));
    std::shared_ptr<Json::Object> g = gmm.toJson();
    obj->put("gmm", g);
    obj->put("theta", theta);
    return obj;

}

OpenWorldUnigram &OpenWorldUnigram::fromJson(const Json::Object &o)
{
    auto type = (Json::String *) o.get("type").get();
    assert(type->data == std::string("OpenWorldUnigram"));

    auto g = ((Json::Object *) o.get("gmm").get());
    gmm.fromJson(*g);
    auto t = ((Json::Number *) o.get("theta").get());
    theta = t->data;

    return *this;
}

}
}
