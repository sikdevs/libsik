#include <bov/unigram.h>
#include <util.h>
#include <sstream>

using namespace Eigen;
using namespace std;

namespace SIK
{
namespace BOV
{

ArrayXd Unigram::_transformSoftAssignment(const ArrayXXd &X) const
{
    ArrayXd out = ArrayXd::Zero(kmeans.K);

    for (int i = X.rows() - 1; i >= 0; --i) {
        out += kmeans.transform(X.row(i));
    }

    return out / double(X.rows());
}

ArrayXd Unigram::_transformHardAssignment(const ArrayXXd &X) const
{
    ArrayXd out = ArrayXd::Zero(kmeans.K);

    for (int i = X.rows() - 1; i >= 0; --i) {
        int k = kmeans.predict(X.row(i));
        out(k) += 1;
    }

    return out / double(X.rows());
}

std::shared_ptr<Json::Object> Unigram::toJson() const
{
    std::shared_ptr<Json::Object> obj = std::make_shared<Json::Object>();
    obj->put("type", std::string("Unigram"));
    std::shared_ptr<Json::Object> k = kmeans.toJson();
    obj->put("kmeans", k);
    return obj;
}

Unigram &Unigram::fromJson(const Json::Object &o)
{
    auto type = (Json::String *) o.get("type").get();
    assert(type->data == std::string("Unigram"));
    auto k = ((Json::Object *) o.get("kmeans").get());
    kmeans.fromJson(*k);
    return *this;
}

}
}
