#include <bov/gmmunigram.h>
#include <util.h>
#include <string.h>

using namespace Eigen;
using namespace std;

namespace SIK
{
namespace BOV
{

ArrayXd GMMUnigram::_transformSoftAssignment(const ArrayXXd &X) const
{
    ArrayXd out = ArrayXd::Zero(gmm.K);

    for (int i = X.rows() - 1; i >= 0; --i) {
        out += gmm.transform(X.row(i));
    }

    return out / double(X.rows());
}

ArrayXd GMMUnigram::_transformHardAssignment(const ArrayXXd &X) const
{
    ArrayXd out = ArrayXd::Zero(gmm.K);

    for (int i = X.rows() - 1; i >= 0; --i) {
        int k = gmm.predict(X.row(i));
        out(k) += 1;
    }

    return out / double(X.rows());
}

std::shared_ptr<Json::Object> GMMUnigram::toJson() const
{
    std::shared_ptr<Json::Object> obj = std::make_shared<Json::Object>();
    obj->put("type", std::string("GMMUnigram"));
    std::shared_ptr<Json::Object> g = gmm.toJson();
    obj->put("gmm", g);
    return obj;
}

GMMUnigram &GMMUnigram::fromJson(const Json::Object &o)
{
    auto type = (Json::String *) o.get("type").get();
    assert(type->data == std::string("GMMUnigram"));

    auto g = ((Json::Object *) o.get("gmm").get());
    gmm.fromJson(*g);
    return *this;
}

}
}
