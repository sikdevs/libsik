#include <trees/decisionTree.h>
#include <util.h>
#include <functional>
#include <algorithm>

using namespace Eigen;
using namespace std;

namespace SIK
{
namespace Trees
{

namespace
{
ArrayXd normalizedHistogram(const ArrayXi &y, const vector<int> &labels)
{
    ArrayXd ret = ArrayXd::Zero(labels.size());

    for (int i = 0; i < y.rows(); ++i) {
        ret[y[i]] += 1;
    }

    ret /= double(y.rows());
    return ret;
}

inline double giniImpurity(const ArrayXi &y, const vector<int> &labels)
{
    return 1 - normalizedHistogram(y, labels).pow(2).sum();
}

double impurityMeasure(const ArrayXd &col, const ArrayXi &y, double threshold,
                       const vector<int> &labels)
{
    int smaller = std::count_if(col.data(),
    col.data() + col.rows(), [threshold](const double i) {
        return i < threshold;
    });
    ArrayXi left(smaller), right(col.rows() - smaller);
    int lc = 0, rc = 0;

    for (int k = 0; k < col.size(); ++k) {
        if (col[k] < threshold) {
            left(lc++) = y[k];
        } else {
            right(rc++) = y[k];
        }
    }

    return giniImpurity(left, labels) + giniImpurity(right, labels);
}

double findBestSplit(const ArrayXXd &X, const ArrayXi &y, int &featureIndex,
                     double &threshold, const vector<int> &labels)
{
    double impurity = numeric_limits<double>::max();

    for (int i = 0; i < X.cols(); ++i) {
        ArrayXd col = X.col(i).eval();
        vector<double> features(col.data(), col.data() + col.size());

        for (int quantile = 1; quantile < 10; ++quantile) {
            size_t offset = size_t(double(features.size() * double(quantile / 10.)));
            nth_element(features.begin(), features.begin() + offset, features.end());
            double m = impurityMeasure(col, y, features[offset], labels);

            if (m < impurity) {
                impurity = m;
                featureIndex = i;
                threshold = features[offset];
            }
        }
    }

    return impurity;
}

template<typename T>
shared_ptr<Node<T>> fitHelper(shared_ptr<Node<T>> parent,
                              const ArrayXXd &X, const ArrayXi &y, int depth, size_t max_depth,
                              double min_impurity_gain, int N_min, const vector<int> &labels,
                              std::function<void(T &, const ArrayXXd &, const ArrayXi &)> setExtraParameters)
{
    auto node = make_shared<Node<T>>(depth);
    node->parent = parent;
    node->data.classDistribution = normalizedHistogram(y, labels);
    double impurity = findBestSplit(X, y, node->data.featureIndex, node->data.threshold,
                                    labels);
    double impurity_gain = giniImpurity(y, labels) - impurity;

    if (depth >= int(max_depth) || X.rows() < N_min
            || impurity_gain < min_impurity_gain) {
        return node;
    }

    setExtraParameters(node->data, X, y);

    std::vector<int> idx_left, idx_right;

    for (int i = 0; i < X.rows(); ++i) {
        if (X(i, node->data.featureIndex) < node->data.threshold) {
            idx_left.push_back(i);
        } else {
            idx_right.push_back(i);
        }
    }

    node->child.push_back(
        fitHelper(node,
                  Util::EigenTools::indexedRowAccess(X, idx_left.begin(), idx_left.end()),
                  Util::EigenTools::indexedRowAccess(y, idx_left.begin(), idx_left.end()),
                  depth + 1, max_depth, min_impurity_gain, N_min, labels, setExtraParameters));

    node->child.push_back(
        fitHelper(node,
                  Util::EigenTools::indexedRowAccess(X, idx_right.begin(), idx_right.end()),
                  Util::EigenTools::indexedRowAccess(y, idx_right.begin(), idx_right.end()),
                  depth + 1, max_depth, min_impurity_gain, N_min, labels, setExtraParameters));

    return node;
}

template<typename T>
ArrayXd transformHelper(shared_ptr<Node<T>> node, const ArrayXd &x,
                        std::function<bool(T &, const ArrayXd &)> accept)
{
    if (!accept(node->data, x)) {
        ArrayXd notAccept = ArrayXd(1);
        notAccept << -1;
        return notAccept;
    }

    if (node->isLeaf()) {
        return node->data.classDistribution;
    }

    int i = int(x[node->data.featureIndex] < node->data.threshold);
    return transformHelper(node->child[i], x, accept);
}

} // anonymous namespace

template<>
DecisionTree<BasicSplit> &DecisionTree<BasicSplit>::fit(
    const ArrayXXd &X, const ArrayXi &y)
{
    labels = Util::EigenTools::toUniqueVector(y);
    root = fitHelper<BasicSplit>(shared_ptr<Node<BasicSplit>>(nullptr), X, y,
                                 0, max_depth, min_impurity_gain, N_min, labels,
    [](BasicSplit &, const ArrayXXd &, const ArrayXi &) {
        // move along, there is nothing to see!
    });
    return *this;
}

template<>
DecisionTree<MinmaxRejectionSplit> &DecisionTree<MinmaxRejectionSplit>::fit(
    const ArrayXXd &X, const ArrayXi &y)
{
    using Ptr = shared_ptr<Node<MinmaxRejectionSplit>>;
    labels = Util::EigenTools::toUniqueVector(y);
    root = fitHelper<MinmaxRejectionSplit>(Ptr(nullptr),
                                           X, y, 0, max_depth, min_impurity_gain, N_min, labels,
    [](MinmaxRejectionSplit & s, const ArrayXXd & Xs, const ArrayXi &) {
        s.lower = Xs.col(s.featureIndex).minCoeff();
        s.upper = Xs.col(s.featureIndex).maxCoeff();
    });
    return *this;
}

template<>
DecisionTree<GaussianRejectionSplit> &DecisionTree<GaussianRejectionSplit>::fit(
    const ArrayXXd &X, const ArrayXi &y)
{
    using Ptr = shared_ptr<Node<GaussianRejectionSplit>>;
    labels = Util::EigenTools::toUniqueVector(y);
    root = fitHelper<GaussianRejectionSplit>(Ptr(nullptr),
            X, y, 0, max_depth, min_impurity_gain, N_min, labels,
    [](GaussianRejectionSplit & s, const ArrayXXd & Xs, const ArrayXi &) {
        s.mean = Xs.col(s.featureIndex).mean();
        s.std = Util::EigenTools::std(Xs.col(s.featureIndex));
    });
    return *this;
}

template<>
DecisionTree<MahalanobisRejectionSplit>
&DecisionTree<MahalanobisRejectionSplit>::fit(const ArrayXXd &X, const ArrayXi &y)
{
    using Ptr = shared_ptr<Node<MahalanobisRejectionSplit>>;
    labels = Util::EigenTools::toUniqueVector(y);
    root = fitHelper<MahalanobisRejectionSplit>(Ptr(nullptr),
            X, y, 0, max_depth, min_impurity_gain, N_min, labels,
    [](MahalanobisRejectionSplit & s, const ArrayXXd & Xs, const ArrayXi &) {
        Eigen::MatrixXd cov = Util::CovarianceEstimation::oas(Xs).matrix();
        //auto cov = Util::CovarianceEstimation::empirical_covariance(Xs).matrix();
        s.mean = Xs.colwise().mean();
        s.precision = Util::EigenTools::pinv(cov);
        s.rejectionThreshold =
            5; // TODO: implement chi-squared CDF, set threshold accordingly
    });
    return *this;
}

template<>
ArrayXd DecisionTree<BasicSplit>::transform(const ArrayXd &x) const
{
    return transformHelper<BasicSplit>(root, x, [&x](BasicSplit &, const ArrayXd &) {
        return true;
    });
}

template<>
ArrayXd DecisionTree<MinmaxRejectionSplit>::transform(const ArrayXd &x) const
{
    return transformHelper<MinmaxRejectionSplit>(root, x,
    [](MinmaxRejectionSplit & s, const ArrayXd & x) {
        return x[s.featureIndex] > s.lower && x[s.featureIndex] < s.upper;
    });
}

template<>
ArrayXd DecisionTree<GaussianRejectionSplit>::transform(const ArrayXd &x) const
{
    return transformHelper<GaussianRejectionSplit>(root, x,
    [](GaussianRejectionSplit & s, const ArrayXd & x) {
        return abs(x[s.featureIndex] - s.mean) <= s.std * 2; // TODO: parametrize?
    });
}

template<>
ArrayXd DecisionTree<MahalanobisRejectionSplit>::transform(const ArrayXd &x) const
{
    return transformHelper<MahalanobisRejectionSplit>(root, x,
    [](MahalanobisRejectionSplit & s, const ArrayXd & x) {
        return Util::Distance::sqmahalanobis(x, s.mean, s.precision) <= s.rejectionThreshold;
    });
}


// serialization
std::shared_ptr<Json::Object> Split::toJson() const
{
    std::shared_ptr<Json::Object> obj(new Json::Object());
    obj->put("feature", featureIndex)->put("threshold", threshold);
    return obj;
}

Split &Split::fromJson(const Json::Object &o)
{
    featureIndex = int(((Json::Number *) o.get("feature").get())->data);
    threshold = ((Json::Number *) o.get("threshold").get())->data;
    return *this;
}

std::shared_ptr<Json::Object> BasicSplit::toJson() const
{
    auto obj = ((Split *)this)->toJson();
    obj->put("type", "BasicSplit");
    return obj;
}

BasicSplit &BasicSplit::fromJson(const Json::Object &o)
{
    auto s = (Json::String *)o.get("type").get();

    if (s->data != "BasicSplit") {
        throw std::runtime_error("Invalid split type. Expected BasicSplit, got " + s->data);
    }

    static_cast<Split *>(this)->fromJson(o);

    return *this;
}

std::shared_ptr<Json::Object> MinmaxRejectionSplit::toJson() const
{
    auto obj = ((Split *)this)->toJson();
    obj->put("type", "MinmaxRejectionSplit")->put("upper", upper)->put("lower", lower);
    return obj;
}

MinmaxRejectionSplit &MinmaxRejectionSplit::fromJson(const Json::Object &o)
{
    auto s = (Json::String *)o.get("type").get();

    if (s->data != "MinmaxRejectionSplit") {
        throw std::runtime_error("Invalid split type. Expected MinmaxRejectionSplit, got " +
                                 s->data);
    }

    upper = ((Json::Number *) o.get("upper").get())->data;
    lower = ((Json::Number *) o.get("lower").get())->data;

    static_cast<Split *>(this)->fromJson(o);
    return *this;
}

std::shared_ptr<Json::Object> GaussianRejectionSplit::toJson() const
{
    auto obj = ((Split *)this)->toJson();
    obj->put("type", "GaussianRejectionSplit")->put("mean", mean)->put("std", std);
    return obj;
}

GaussianRejectionSplit &GaussianRejectionSplit::fromJson(const Json::Object &o)
{
    auto s = (Json::String *)o.get("type").get();

    if (s->data != "GaussianRejectionSplit") {
        throw std::runtime_error("Invalid split type. Expected GaussianRejectionSplit, got "
                                 + s->data);
    }

    mean = ((Json::Number *) o.get("mean").get())->data;
    std = ((Json::Number *) o.get("std").get())->data;

    static_cast<Split *>(this)->fromJson(o);
    return *this;
}

std::shared_ptr<Json::Object> MahalanobisRejectionSplit::toJson() const
{
    auto obj = ((Split *)this)->toJson();
    obj->put("type", "MahalanobisRejectionSplit");
    obj->put("mean", std::make_shared<Json::Array>(mean));
    obj->put("precision", std::make_shared<Json::Array>(precision));
    return obj;
}

MahalanobisRejectionSplit &MahalanobisRejectionSplit::fromJson(const Json::Object &o)
{
    auto s = (Json::String *)o.get("type").get();

    if (s->data != "MahalanobisRejectionSplit") {
        throw std::runtime_error("Invalid split type. Expected MahalanobisRejectionSplit, got "
                                 + s->data);
    }

    mean = ((Json::Array *) o.get("mean").get())->eigenArrayXd();
    precision = ((Json::Array *) o.get("precision").get())->eigenArrayXXd();

    static_cast<Split *>(this)->fromJson(o);
    return *this;
}

}
}
