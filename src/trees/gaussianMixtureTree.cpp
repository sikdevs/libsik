#include <cluster/gmm.h>
#include <trees/tree.h>
#include <trees/gaussianMixtureTree.h>
#include <util.h>
#include <sstream>
#include <iostream>
#include <set>
#include <stack>
#include <memory>

using namespace Eigen;
using namespace std;
using namespace SIK::Util::Distance;
using namespace SIK::Util::PDF;

namespace SIK
{
namespace Trees
{

shared_ptr<Json::Object> GaussianMixtureTree::TreeData::toJson() const
{
    std::shared_ptr<Json::Object> json = std::make_shared<Json::Object>();
    json->put("type", std::string("GMT_Data"));
    std::shared_ptr<Json::JsonBase> m = std::make_shared<Json::Array>(mean);
    json->put("mean", m);
    std::shared_ptr<Json::JsonBase> c = std::make_shared<Json::Array>(cov);
    json->put("cov", c);
    json->put("prior", prior);
    json->put("theta", theta);
    return json;
}

GaussianMixtureTree::TreeData &GaussianMixtureTree::TreeData::fromJson(
    const Json::Object &o)
{
    auto type = (Json::String *) o.get("type").get();
    assert(type->data == std::string("GMT_Data"));
    auto p = ((Json::Number *) o.get("prior").get());
    prior = p->data;
    auto t = ((Json::Number *) o.get("theta").get());
    theta = t->data;
    auto m = ((Json::Array *) o.get("mean").get());
    mean = m->eigenArrayXXd();
    auto c = ((Json::Array *) o.get("cov").get());
    cov = c->eigenArrayXXd();

    prec = cov.matrix().inverse();
    logDetPi = std::log(cov.matrix().determinant()) - std::log(prior * prior);
    return *this;
}

using GMTNode = GaussianMixtureTree::GMTNode;
using GMTNodePtr = shared_ptr<GaussianMixtureTree::GMTNode>;

namespace
{
double computeTheta(Eigen::ArrayXd distances, double rho)
{
    double upper = distances.maxCoeff(), lower = distances.minCoeff(), theta;
    size_t target = size_t(rho * distances.rows());

    for (size_t i = 0; i < 50; ++i) {
        //binary search
        theta = (upper - lower) / 2;
        size_t nRejected = (distances > theta).sum();

        if (nRejected > target) {
            upper = theta;
        } else if (nRejected < target) {
            lower = theta;
        } else {
            break;
        }
    }

    return (upper - lower) / 2;
}

GMTNodePtr fitNode(GMTNodePtr parent, const ArrayXXd &X,
                   const ArrayXd &mean, const ArrayXXd &cov, double prior,
                   int depth, int max_depth, int min_samples, size_t K,
                   double inlierFraction)
{
    auto node = make_shared<GMTNode>(GaussianMixtureTree::TreeData(mean, cov, prior),
                                     depth);
    node->parent = parent;

    ArrayXd distances(X.rows());

    for (size_t i = 0; i < size_t(X.rows()); ++i) {
        distances[i] = Util::Distance::sqmahalanobis(X.row(i), mean, node->data.prec);
    }

    if (depth >= max_depth || X.rows() < min_samples) {
        node->data.theta = computeTheta(distances, inlierFraction);
        return node;
    }

    // inner node: looser bounds
    node->data.theta = computeTheta(distances, 1.0);

    Cluster::GMM gmm = Cluster::GMM(K).fit(X);
    std::vector<std::vector<int>> indices(K);

    for (size_t x = 0; x < size_t(X.rows()); ++x) {
        indices[gmm.predict(X.row(x))].push_back(x);
    }

    for (size_t k = 0; k < K; ++k) {
        double prior = gmm.priors[k];

        if (!std::isnan(prior)) {
            auto X_node = Util::EigenTools::indexedRowAccess(X, indices[k].begin(),
                          indices[k].end());
            node->child.push_back(fitNode(node, X_node,
                                          gmm.means[k], gmm.covariances[k], prior,
                                          depth + 1, max_depth, min_samples, K, inlierFraction));
        }
    }

    return node;
}
}

GaussianMixtureTree &GaussianMixtureTree::fit(const Eigen::ArrayXXd &X,
        double inlierFraction)
{
    auto mean = X.colwise().mean();
    auto cov = Util::CovarianceEstimation::empirical_covariance(X);
    root = fitNode(shared_ptr<GMTNode>(nullptr), X,
                   mean, cov, 1, 0, max_depth, N_min, K, inlierFraction);
    return *this;
}

bool GaussianMixtureTree::predict(const Eigen::ArrayXd &x)
{
    GMTNodePtr node = root;

    auto dist = sqmahalanobis(x, node->data.mean, node->data.prec);

    while (!(node->isLeaf())) {
        if (dist > node->data.theta) {
            return false;
        }

        // else: find child most propable to have generated x
        double d_min = numeric_limits<double>::max();
        GMTNodePtr c_best;

        for (const auto &c : node->child) {
            auto d = sqmahalanobis(x, c->data.mean, c->data.prec) + c->data.logDetPi;

            if (d < d_min) {
                d_min = d;
                c_best = c;
            }
        }

        node = c_best;
        dist = d_min - node->data.logDetPi;
    }

    return dist <= node->data.theta;
}

double GaussianMixtureTree::transform(const Eigen::ArrayXd &x)
{
    GMTNodePtr node = root;

    // Traverse tree in a greedy manner
    while (!(node->isLeaf())) {
        double d_min = numeric_limits<double>::max();
        GMTNodePtr c_best;

        for (const auto &c : node->child) {
            auto d = sqmahalanobis(x, c->data.mean, c->data.prec) + c->data.logDetPi;

            if (d < d_min) {
                d_min = d;
                c_best = c;
            }
        }

        node = c_best;
    }

    return mvNormal(x, node->data.mean, node->data.prec,
                    node->data.det_cov) * node->data.prior;
}

std::shared_ptr<Json::Object> GaussianMixtureTree::toJson() const
{
    std::shared_ptr<Json::Object> obj(new Json::Object);
    obj->put("type", std::string("GMT"));
    std::shared_ptr<Json::Object> rootData = root->toJson();
    obj->put("root", rootData);
    return obj;
}

GaussianMixtureTree &GaussianMixtureTree::fromJson(const Json::Object &o)
{
    auto type = (Json::String *) o.get("type").get();

    if (type->data != "GMT") {
        throw std::runtime_error("Invalid object type. Expected GMT, got " +
                                 type->data);
    }

    root = std::make_shared<Node<TreeData>>(0);
    root->fromJson(*((Json::Object *) o.get("root").get()));
    return *this;
}
}
}
