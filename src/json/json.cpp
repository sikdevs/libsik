#include <json/json.h>
#include <Eigen/Dense>
#include <vector>
#include <map>
#include <memory>
#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept>

namespace SIK
{
namespace Json
{
std::string Null::str()
{
    return "null";
}
std::string Number::str()
{
    return std::to_string(data);
}
std::string Bool::str()
{
    return data ? "true" : "false";
}
std::string String::str()
{
    return "\"" + data + "\"";
}
std::string Object::str()
{
    std::stringstream ss;

    for (auto it = data.begin(); it != data.end(); it++) {
        ss << "\"" << it->first << "\"" << ":" << it->second->str() << ",";
    }

    return "{" + ss.str().substr(0, ss.str().size() - 1) + "}";
}
Eigen::ArrayXd Array::eigenArrayXd()
{
    Eigen::ArrayXd result(data.size());

    for (size_t i = 0; i < data.size(); ++i) {
        result(i) = std::static_pointer_cast<Number>(data[i])->data;
    }

    return result;
}
Eigen::ArrayXXd Array::eigenArrayXXd()
{
    if (data.size() > 0) {
        try {
            Eigen::ArrayXXd result(data.size(),
                                   (std::static_pointer_cast<Array>(data[0])->data.size()));

            for (size_t i = 0; i < data.size(); ++i) {
                for (size_t j = 0; j < std::static_pointer_cast<Array>(data[i])->data.size(); ++j) {
                    result(i, j) = std::static_pointer_cast<Number>(std::static_pointer_cast<Array>
                                   (data[i])->data[j])->data;
                }
            }

            return result;
        } catch (const std::exception &) {
            throw std::runtime_error("Could not convert to Eigen::ArrayXXD: \"" + str() + "\"");
        }
    } else {
        Eigen::ArrayXXd result(0, 0);
        return result;
    }
}
std::string Array::str()
{
    std::stringstream ss;
    ss << "[";

    if (data.size() > 0) {
        ss <<  data[0]->str();

        for (size_t d = 1; d < data.size(); ++d) {
            ss << "," + data[d]->str();
        }
    }

    ss << "]";
    return ss.str();
}
Object *Object::put(std::string name)
{
    data[name] = std::shared_ptr<Null>(new Null());
    return this;
}
Object *Object::put(std::string name, const double obj)
{
    data[name] = std::shared_ptr<Number>(new Number(obj));
    return this;
}
Object *Object::put(std::string name, const bool obj)
{
    data[name] = std::shared_ptr<Bool>(new Bool(obj));
    return this;
}
Object *Object::put(std::string name, const std::string obj)
{
    data[name] = std::shared_ptr<String>(new String(obj));
    return this;
}
Object *Object::put(std::string name, const std::shared_ptr<JsonBase> &obj)
{
    data[name] = obj;
    return this;
}
void Array::push_back(const int obj)
{
    data.push_back(std::shared_ptr<Number>(new Number(obj)));
}
void Array::push_back(const double obj)
{
    data.push_back(std::shared_ptr<Number>(new Number(obj)));
}
void Array::push_back(const bool obj)
{
    data.push_back(std::shared_ptr<Bool>(new Bool(obj)));
}
void Array::push_back(const std::string obj)
{
    data.push_back(std::shared_ptr<String>(new String(obj)));
}
void Array::push_back(const std::shared_ptr<JsonBase> &obj)
{
    data.push_back(obj);
}

std::shared_ptr<String> String::parse(std::stringstream &ss)
{
    char c = char(ss.get());
    std::ostringstream oss;

    if (c != '"') {
        throw std::runtime_error("could not parse string: \"" + oss.str() + "\"");
    }

    // else
    while (ss) {
        c = char(ss.get());

        if (c == '"') {
            break;
        } else {
            oss << c;
        }
    }

    return std::shared_ptr<String>(new String(oss.str()));
}

std::shared_ptr<Bool> Bool::parse(std::stringstream &ss)
{
    std::string t = "true";
    std::string f = "false";
    char c = char(ss.peek());
    std::ostringstream oss;

    while (c != ',' && c != ']' && c != '}' && ss) {
        oss << char(ss.get());
        c = char(ss.peek());
    }

    std::string s(oss.str());

    if (s == t) {
        return std::shared_ptr<Bool>(new Bool(true));
    } else if (s == f) {
        return std::shared_ptr<Bool>(new Bool(false));
    } else {
        std::cout << "error: could not parse\"" + s + "\"";
        throw std::runtime_error("cannot parse bool: " + s);
    }
}

std::shared_ptr<Number> Number::parse(std::stringstream &ss)
{
    char c = char(ss.peek());
    std::ostringstream oss;

    while (c != ',' && c != ']' && c != '}' && ss) {
        oss << char(ss.get());
        c = char(ss.peek());
    }

    size_t tmp;
    double value = std::stod(oss.str(), &tmp);
    return std::shared_ptr<Number>(new Number(value));
}

std::shared_ptr<Null> Null::parse(std::stringstream &ss)
{
    std::ostringstream oss;

    for (size_t i = 0; i < 4; ++i) {
        oss << ss.get();
    }

    std::string s(oss.str());

    if (s == "null") {
        return std::shared_ptr<Null>(new Null());
    } else {
        throw std::runtime_error("could not parse null: \"" + s + "\"");
    }
}
std::shared_ptr<Array> Array::parse(std::stringstream &ss)
{
    std::shared_ptr<Array> array(new Array());
    char c = char(ss.get());

    do {
        c = char(ss.peek());

        if (c == '"') {
            array->push_back(String::parse(ss));
        } else if (c == 't' || c == 'f') {
            array->push_back(Bool::parse(ss));
        } else if ((c >= '0' && c <= '9') || c == '-' || c == '.') {
            array->push_back(Number::parse(ss));
        } else if (c == 'n') {
            array->push_back(Null::parse(ss));
        } else if (c == '[') {
            array->push_back(Array::parse(ss));
        } else if (c == '{') {
            array->push_back(Object::parse(ss));
        } else if (c == ']') {
            ss.get();
            break;
        } else {
            char problem[30];
            ss.getline(problem, 30);
            throw std::runtime_error("could not parse array element \"" + std::string(
                                         problem) + "\" in\n\"" + ss.str() + "\"");
        }
    } while (ss.get() == ',');

    return array;
}

std::shared_ptr<JsonBase> Object::get(std::string name)const
{
    auto it = data.find(name);

    if (it == data.end()) {
        throw std::runtime_error("could not find \"" + name + "\" in Json::Object");
    } else {
        return it->second;
    }
}

inline void Object::parsePair(std::stringstream &ss, std::shared_ptr<Object> o)
{
    std::string key = String::parse(ss)->data;

    if (ss.get() == ':') {
        char c = char(ss.peek());

        if (c == '"') {
            o->put(key, String::parse(ss));
        } else if (c == 't' || c == 'f') {
            o->put(key, Bool::parse(ss));
        } else if ((c >= '0' && c <= '9') || c == '-' || c == '.') {
            o->put(key, Number::parse(ss));
        } else if (c == 'n') {
            o->put(key, Null::parse(ss));
        } else if (c == '[') {
            o->put(key, Array::parse(ss));
        } else if (c == '{') {
            o->put(key, Object::parse(ss));
        } else {
            throw std::runtime_error("could not parse value");
        }
    } else {
        throw std::runtime_error("could not parse pair");
    }
}

std::shared_ptr<Object> Object::parse(const std::string &s)
{
    std::stringstream ss;
    ss << s;
    return parse(ss);
}

std::shared_ptr<Object> Object::parse(std::stringstream &ss)
{
    std::shared_ptr<Object> object(new Object());
    char c = char(ss.get());
    c = char(ss.peek());
    int i = 0;

    while (c != '}' && ss) {
        if (c == ',' || ++i >= 0) {
            parsePair(ss, object);
            c = char(ss.get());
        } else {
            throw std::runtime_error("could not parse object: \"" + ss.str() + "\"");
        }
    }

    return object;
}
}
}
