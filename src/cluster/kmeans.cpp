#include <json/json.h>
#include <cluster/kmeans.h>
#include <util.h>
#include <limits>
#include <string>
#include <sstream>
#include <random>
#include <algorithm>
using namespace Eigen;
using namespace std;

namespace SIK
{
namespace Cluster
{
// K-Means++ initialization
KMeans &KMeans::intializeCenters(const ArrayXXd &X)
{
    D = X.cols(); // important: set expected input dimension
    const size_t N = X.rows();

    random_device rd;
    mt19937_64 rand_gen(rd());

    // (1) choose first seed point m_1 from X at random
    means.clear();
    means.reserve(K);
    means.push_back(X.row(uniform_int_distribution<>(0, N - 1)(rand_gen)));

    ArrayXd dists = ArrayXd::Constant(N, numeric_limits<double>::infinity());

    for (size_t k = 1; k < K; ++k) {
        assert(means.size() == k);

        // (2) compute minimum distances D(x) = min ||x-m_i|| for all x in X
        double cumulative_dist = 0;

        for (size_t n = 0; n < N; ++n) {
            cumulative_dist += dists[n] = std::min(dists[n],
                                                   Util::Distance::sqeuclidean(X.row(n), means.back()));
        }

        // (3) sample new seed from X with probability D(x)^2
        double d = uniform_real_distribution<>(.0, cumulative_dist)(rand_gen);

        for (size_t n = N; n > 0; --n) {
            cumulative_dist -= dists[n - 1];

            if (cumulative_dist <= d) {
                means.push_back(X.row(n - 1));
                break;
            }
        }

        assert(means.size() == k + 1);
    }

    return *this;
}

// Lloyd's algorithm for K-Means clustering
KMeans &KMeans::findClustering(const ArrayXXd &X, size_t n_iter, double eps)
{
    assert(means.size() == K); // TODO: throw exception instead of assert

    D = X.cols(); // important: set expected input dimension
    const size_t N = X.rows();

    // scale eps by number of clusters to avoid division in loop below
    eps *= K;

    // means of last iteration to check if algorithm converges
    std::vector<Eigen::VectorXd> means_last;
    means_last.reserve(K);

    for (size_t k = 0; k < K; ++k) {
        means_last.push_back(ArrayXd::Zero(D));
    }

    // cluster membership of each x in X
    Matrix<size_t, Dynamic, 1> membership(X.size());
    // number of x in cluster k
    Matrix<size_t, Dynamic, 1> count = Matrix<size_t, Dynamic, 1>::Zero(K);

    for (size_t step = 0; step < n_iter; ++step) {
        // assignment step
        count.setZero();

        #pragma omp parallel for

        for (int i = 0; size_t(i) < N; ++i) {
            const size_t k = Util::nearest(means, X.row(i), Util::Distance::sqeuclidean);
            membership[i] = k;
        }

        for (size_t i = 0; i < N; ++i) {
            auto k = membership[i];
            count[k]++;
        }

        // update step
        std::swap(means, means_last);

        for (size_t k = 0; k < K; ++k) {
            means[k].setZero();
        }

        for (size_t i = 0; i < N; ++i) {
            const size_t k = membership[i];
            means[k] += X.row(i).matrix();
        }

        // convergence?
        double centers_change = 0;

        for (size_t k = 0; k < K; ++k) {
            if (count[k] > 0) {
                means[k] /= double(count[k]);
            }

            centers_change += Util::Distance::sqeuclidean(means[k], means_last[k]);
        }

        if (centers_change < eps) {
            break;
        }
    }

    return *this;
}

int KMeans::predict(const ArrayXd &x) const
{
    assert(x.size() == int(D));
    return Util::nearest(means, x, Util::Distance::sqeuclidean);
}

ArrayXd KMeans::transform(const ArrayXd &x) const
{
    ArrayXd out = ArrayXd::Zero(K);

    for (size_t k = 0; k < K; ++k) {
        out(k) = Util::Distance::euclidean(means[k], x);
    }

    return out;
}

std::shared_ptr<Json::Object> KMeans::toJson() const
{
    auto obj = make_shared<Json::Object>();
    obj->put("type", std::string("KMeans"));
    obj->put("K", K);
    obj->put("D", D);
    std::shared_ptr<Json::JsonBase> m = std::make_shared<Json::Array>(means);
    obj->put("means", m);
    return obj;
}

KMeans &KMeans::fromJson(const Json::Object &o)
{
    auto type = (Json::String *) o.get("type").get();
    assert(type->data == std::string("KMeans"));

    auto K_val = (Json::Number *) o.get("K").get();
    auto D_val = (Json::Number *) o.get("D").get();
    auto m = ((Json::Array *) o.get("means").get());

    K = size_t(K_val->data);
    D = size_t(D_val->data);

    for (size_t k = 0; k < K; ++k) {
        Eigen::ArrayXd m_row = ((Json::Array *) m->data[k].get())->eigenArrayXXd();
        means.push_back(m_row);
    }

    return *this;
}
}
}
