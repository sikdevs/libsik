#include <json/json.h>
#include <cluster/gmm.h>
#include <cluster/kmeans.h>
#include <util.h>
#include <limits>
#include <string>
#include <math.h>
#include <sstream>
#include <memory>
#include <iostream>

using namespace Eigen;
using namespace std;
using namespace SIK::Util::PDF;

namespace SIK
{
namespace Cluster
{
// https://www.ics.uci.edu/~smyth/courses/cs274/notes/EMnotes.pdf
GMM &GMM::fit(const ArrayXXd &X, size_t n_iter, double eps)
{
    //init [[[
    D = X.cols();
    size_t N = X.rows();
    KMeans kmeans = KMeans(K);
    kmeans.fit(X);
    means = kmeans.means;

    for (size_t k = 0; k < K; ++k) {
        priors.push_back(1.0 / K);
        MatrixXd centered = X.rowwise() - X.colwise().mean();
        covariances.push_back((centered.adjoint() * centered) /
                              double (X.rows() - 1));
        precisions.push_back(covariances[k].inverse());
        det_covs.push_back(covariances[k].determinant());
    }

    ArrayXXd w_ik = ArrayXXd(N, K);
    double e, e_last = numeric_limits<double>::max();
    // ]]]

    for (size_t step = 0; step < n_iter; ++step) {
        //expectation [[[

        for (size_t i = 0; i < N; ++i) {
            for (size_t k = 0; k < K; ++k) {
                double divisor = 0;

                for (size_t m = 0; m < K; ++m) {
                    divisor += w_ik(i, k) = mvNormal(X.row(i), means[m], precisions[m],
                                                     det_covs[m]) * priors[m];
                }

                for (size_t m = 0; m < K; ++m) {
                    w_ik(i, k) /= divisor;
                }
            }
        }

        // ]]]
        //maximization [[[
        auto N_k = (w_ik.colwise().sum());

        for (size_t k = 0; k < K; ++k) {
            priors[k] = N_k(k) / N;
            means[k] *= 0;

            for (size_t i = 0; i < N; ++i) {
                means[k] += (w_ik(i, k) * X.row(i).matrix());
            }

            means[k] /= N_k(k);
            covariances[k] *= 0;

            for (size_t i = 0; i < N; ++i) {
                covariances[k] += w_ik(i, k) * (X.row(i).transpose().matrix() - means[k].matrix())
                                  * (X.row(i).matrix() - means[k].transpose().matrix());
            }

            covariances[k] /= N_k(k);
        }

        // ]]]
        //convergence [[[
        e = 0.0;
        double tmp = 0.0;

        for (size_t k = 0; k < K; ++k) {
            precisions[k] = covariances[k].inverse();
            det_covs[k] = covariances[k].determinant();
        }

        for (size_t i = 0; i < N; ++i) {
            tmp = 0.0;

            for (size_t k = 0; k < K; ++k) {
                tmp += priors[k] * mvNormal(X.row(i), means[k], precisions[k], det_covs[k]);
            }

            e += log(tmp);
        }

        if (abs(e_last - e) < eps) {
            break;
        } else {
            e_last = e;
        }

        // ]]]
    }

    return *this;
}
int GMM::predict(const ArrayXd &X) const
{
    assert(X.size() == int (D));
    size_t index;
    transform(X).maxCoeff(&index);
    return index;
}
ArrayXd GMM::transform(const ArrayXd &x)const
{
    ArrayXd out = ArrayXd::Zero(K);

    for (size_t k = 0; k < K; ++k) {
        out(k) = priors[k] * mvNormal(x, means[k], precisions[k], det_covs[k]);
    }

    return out;
}

std::shared_ptr<Json::Object> GMM::toJson()const
{
    auto json = std::make_shared<Json::Object>();
    json->put("type", std::string("GMM"));
    json->put("K", K);
    json->put("D", D);
    json->put("priors", std::make_shared<Json::Array>(priors));
    json->put("means", std::make_shared<Json::Array>(means));
    json->put("covariances", std::make_shared<Json::Array>(covariances));
    return json;
}
GMM &GMM::fromJson(const Json::Object &obj)
{
    auto type = (Json::String *) obj.get("type").get();
    assert(type->data == std::string("GMM"));

    auto K_val = (Json::Number *) obj.get("K").get();
    auto D_val = (Json::Number *) obj.get("D").get();
    auto p = ((Json::Array *) obj.get("priors").get())->eigenArrayXd();
    auto m = ((Json::Array *) obj.get("means").get());
    auto c = ((Json::Array *) obj.get("covariances").get());

    K = size_t(K_val->data);
    D = size_t(D_val->data);

    for (size_t k = 0; k < K; ++k) {
        priors.push_back(p(k));

        Eigen::ArrayXd m_row = ((Json::Array *) m->data[k].get())->eigenArrayXXd();
        means.push_back(m_row);

        Eigen::ArrayXXd c_mat = ((Json::Array *) c->data[k].get())->eigenArrayXXd();
        covariances.push_back(c_mat);

        precisions.push_back(c_mat.matrix().inverse());
        det_covs.push_back(c_mat.matrix().determinant());
    }

    return *this;
}
}
}
