#include <util.h>
#include <random>
#include <set>
#include <map>

namespace SIK
{
namespace Util
{
namespace Evaluation
{
std::vector<std::vector<int>> stratifiedKFoldSet(const int k,
                           const Eigen::ArrayXd &y)
{
    assert(k >= 1);
    std::map<double, std::vector<int>> classBucket;

    for (size_t r = 0; r < size_t(y.rows()); ++r) {
        classBucket[y(r)].push_back(r);
    }

    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::vector<std::vector<int>> skfs;
    std::set<int> used;

    for (size_t s = 0; s < size_t(k - 1); ++s) {
        std::vector<int> testset;

        for (auto c = classBucket.begin(); c != classBucket.end(); ++c) {
            size_t n_elements = size_t(c->second.size() / (k - s) + .5); // = round(size()/(k-s))
            std::shuffle(c->second.begin(), c->second.end(), gen);

            for (size_t i = 0; i < n_elements; ++i) {
                testset.push_back(c->second.back());
                c->second.pop_back();
            }
        }

        skfs.push_back(testset);
    }

    std::vector<int> testset;

    for (auto c : classBucket) {
        for (auto t : c.second) {
            testset.push_back(t);
        }

        //testset.insert(testset.end(), c.second.begin(), c.second.begin()+c.second.size());
    }

    skfs.push_back(testset);
    return skfs;
}

}

namespace CovarianceEstimation
{

Eigen::ArrayXXd oas(const Eigen::ArrayXXd &X, bool centered)
{
    int n_samples, n_features;
    auto X_centered = X;

    if (!centered) {
        X_centered = X.rowwise() - X.colwise().mean();
    }

    if (X.cols() == 1) {
        Eigen::ArrayXXd ret(1, 1);
        ret(0, 0) = X_centered.pow(2).mean();
        return ret;
    } else {
        n_samples = X.rows();
        n_features = X.cols();
        Eigen::ArrayXXd cov = empirical_covariance(X_centered, true);
        double mu = cov.matrix().trace() / n_features;

        double alpha = cov.pow(2).mean();
        double num = alpha + std::pow(mu, 2);
        double den = (n_samples + 1) * (alpha - std::pow(mu, 2) / n_features);
        double shrinkage = den == 0 ? 1.0 : std::min(num / den, 1.0);
        cov = (1.0 - shrinkage) * cov;
        cov.col(0) = cov.col(0) + shrinkage * mu;
        return cov;
    }
}
}
}
}