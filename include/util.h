#pragma once

#include <Eigen/Dense>
#include <vector>
#include <ostream>
#include <limits>
#include <memory>
#include <iostream>

namespace SIK
{
/// Assorted utilities that are used across submodules.
namespace Util
{
/// Distance metrics.
namespace Distance
{
/** Squared euclidean distance.
 * @param a First vector of length `d`.
 * @param b Second vector of length `d`.
 * @returns The squared euclidean distance of a and b, ||a-b||_2^2 = (a-b)^T(a-b).
 */
inline double sqeuclidean(const Eigen::VectorXd &a, const Eigen::VectorXd &b)
{
    auto d = a - b;
    return d.dot(d);
}

/** Euclidean distance.
 * @param a First vector of length `d`.
 * @param b Second vector of length `d`.
 * @returns The euclidean distance of a and b, ||a-b||_2 = sqrt((a-b)^T(a-b)).
 */
inline double euclidean(const Eigen::VectorXd &a, const Eigen::VectorXd &b)
{
    return sqrt(sqeuclidean(a, b));
}

/** Squared Mahalanonbis distance.
 * @param a First vector of length `d`.
 * @param b Second vector of length `d`.
 * @param P precision matrix `d`.
 * @returns The squared mahalanobis distance of a and b in respect of S, d(a, b, P) = (a-b)^T P (a-b).
 */
inline double sqmahalanobis(const Eigen::VectorXd &a, const Eigen::VectorXd &b,
                            const Eigen::ArrayXXd &P)
{
    auto d = a - b;
    return d.transpose() * (P.matrix() * d);
}

/** Mahalanonbis distance.
 * @param a First vector of length `d`.
 * @param b Second vector of length `d`.
 * @param P precision matrix `d`.
 * @returns The mahalanobis distance of a and b in respect of S, d(a, b, P) = sqrt((a-b)^T P (a-b)).
 */
inline double mahalanobis(const Eigen::VectorXd &a, const Eigen::VectorXd &b,
                          const Eigen::ArrayXXd &P)
{
    return sqrt(sqmahalanobis(a, b, P));
}
}

namespace PDF
{
/** Multivariate normal distribution.
 * @param x Position where to compute the density.
 * @param mean The mean of the normal distribution.
 * @param precision Precision matrix (inverse of covariance).
 * @param det_cov Determinant of the covariance.
 * @returns Density at x.
 */
inline double mvNormal(const Eigen::ArrayXd &x, const Eigen::ArrayXd &mean,
                       const Eigen::MatrixXd &precision, double det_cov)
{
    auto partition = sqrt(pow(2 * M_PI, mean.rows()) * det_cov);
    return exp(-.5 * Distance::sqmahalanobis(x, mean, precision)) / partition;
}

/** Multivariate normal distribution.
 * @param x Position where to compute the density.
 * @param mean The mean of the normal distribution.
 * @param cov Covariance matrix.
 * @returns Density at x.
 */
inline double mvNormal(const Eigen::ArrayXd &x, const Eigen::ArrayXd &mean,
                       const Eigen::MatrixXd &cov)
{
    return mvNormal(x, mean, cov.inverse(), cov.determinant());
}
}

/** Get index of center nearest to a given vector x.
 * Examples:
 *
 *     // Default distance: squared euclidean
 *     size_t i = nearest(kmeans.means, x);
 *     pos += kmeans.means[i];
 *
 *     // cosine distance
 *     nearest(centers, x, [](const VectorXd& a, const VectorXd& b) {
 *             return a.dot(b)/sqrt(a.dot(a)*b.dot(b)); })
 *
 *     // Mahalanobis distance with fixed precision matrix P
 *     // needs #include <functional>
 *     nearest(centers, x, std::bind(mahalanobis, _1, _2, P));
 *
 * @param centers List of centers to compare x to.
 * @param x The vector to compare centers to.
 * @param dist The distance function to consider. Must take two vector arguments and return a double.
 * @returns Index of the nearest center in `centers`.
 */
template<typename Func>
size_t nearest(const std::vector<Eigen::VectorXd> &centers, const Eigen::VectorXd &x,
               Func dist)
{
    size_t min_id = 0;
    double min_dist = std::numeric_limits<double>::infinity();

    for (size_t k = centers.size(); k > 0; --k) {
        double d = dist(centers[k - 1], x);

        if (d < min_dist) {
            min_dist = d;
            min_id = k - 1;
        }
    }

    return min_id;
}

inline size_t nearest(const std::vector<Eigen::VectorXd> &centers,
                      const Eigen::VectorXd &x)
{
    return nearest(centers, x, Distance::sqeuclidean);
}

// TODO: iterators: rows(array), cols(array)

/** Split a 2D-array row-wise into a vector of 1D-arrays.
 * @param X 2D input array.
 * @returns vector with the rows of X.
 */
template<typename T>
std::vector<Eigen::Array<T, Eigen::Dynamic, 1>> splitRows(const
        Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic> &X)
{
    std::vector<Eigen::Array<T, Eigen::Dynamic, 1>> out;
    out.reserve(X.rows());

    for (int i = 0; i < X.rows(); ++i) {
        out.push_back(X.row(i));
    }

    return out;
}

/** Split a 2D-array column-wise into a vector of 1D-arrays.
 * @param X 2D input array.
 * @returns vector with the columns of X.
 */
template<typename T>
std::vector<Eigen::Array<T, Eigen::Dynamic, 1>> splitCols(const
        Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic> &X)
{
    std::vector<Eigen::Array<T, Eigen::Dynamic, 1>> out;
    out.reserve(X.cols());

    for (int i = 0; i < X.cols(); ++i) {
        out.push_back(X.col(i));
    }

    return out;
}

/** Join a list of 1D-arrays into a 2D-array, row-wise.
 * @param Xs List of 1D-arrays.
 * @returns 2D-array containing Xs as rows.
 */
template<typename T>
Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic> concatRows(const
        std::vector<Eigen::Array<T, Eigen::Dynamic, 1>> &Xs)
{
    assert(Xs.size() > 0); // TODO: throw exception instead of assert
    const int D = Xs[0].size();
    Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic> out(Xs.size(), D);
    int i = 0;

    for (const auto &x : Xs) {
        assert(D == x.size()); // TODO: throw exception instead of assert
        out.row(i) = x;
    }

    return out;
}

/** Join a list of 1D-arrays into a 2D-array, column-wise.
 * @param Xs List of 1D-arrays.
 * @returns 2D-array containing Xs as columns.
 */
template<typename T>
Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic> concatCols(const
        std::vector<Eigen::Array<T, Eigen::Dynamic, 1>> &Xs)
{
    assert(Xs.size() > 0); // TODO: throw exception instead of assert
    const int D = Xs[0].size();
    Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic> out(D, Xs.size());
    int i = 0;

    for (const auto &x : Xs) {
        assert(D == x.size()); // TODO: throw exception instead of assert
        out.col(i) = x;
    }

    return out;
}

/// Serializable class interace.
struct Serializable {
    /// Represent class as string.
    virtual std::string serialize() const = 0;
    /// Initialize object from string. Will overwrite existing object.
    virtual Serializable &deserialize(const std::string &s) = 0;

    virtual ~Serializable() {}
};

inline std::ostream &operator<<(std::ostream &os, const Util::Serializable &s)
{
    return (os << s.serialize());
}

namespace CovarianceEstimation
{
inline Eigen::ArrayXXd empirical_covariance(const Eigen::ArrayXXd &X,
        bool centered = false)
{
    Eigen::ArrayXXd X_centered = centered ? X : X.rowwise() - X.colwise().mean();
    Eigen::ArrayXXd cov = (X_centered.matrix().adjoint() * X_centered.matrix()) /
                          (X.rows() - 1);
    return cov;

}

Eigen::ArrayXXd oas(const Eigen::ArrayXXd &X, bool centered = false);
}

namespace EigenTools
{

template <typename Iterator>
inline Eigen::ArrayXXd booledRowAccess(const Eigen::ArrayXXd &base, Iterator start,
                                       Iterator end)
{
    static_assert(
        std::is_same<typename std::iterator_traits<Iterator>::value_type, bool>::value,
        "ProcessIndexes: Iterators must point to bool.");
    auto a = start;
    Eigen::ArrayXXd ret(count(a, end, true), base.cols());
    int j = 0;

    for (; start != end; ++start) {
        if (*start) {
            ret.row(j++) = base.row(*start);
        }
    }

    return ret;
}

inline std::vector<int> toUniqueVector(const Eigen::ArrayXi &base)
{
    std::vector<int> result(base.data(), base.data() + base.rows());
    std::sort(result.begin(), result.end());
    auto last = std::unique(result.begin(), result.end());
    result.erase(last, result.end());
    return result;
}

inline std::vector<int> toUniqueVector(const Eigen::ArrayXXi &base)
{
    std::vector<int> result(base.data(), base.data() + base.rows() * base.cols());
    std::sort(result.begin(), result.end());
    auto last = std::unique(result.begin(), result.end());
    result.erase(last, result.end());
    return result;
}

inline std::vector<double> toUniqueVector(const Eigen::ArrayXXd &base)
{
    std::vector<double> result(base.data(), base.data() + base.rows() * base.cols());
    std::sort(result.begin(), result.end());
    auto last = std::unique(result.begin(), result.end());
    result.erase(last, result.end());
    return result;
}


template <typename Iterator, typename Array>
inline Array indexedRowAccess(const Array &base, Iterator start, Iterator end)
{
    static_assert(
        std::is_same<typename std::iterator_traits<Iterator>::value_type, int>::value,
        "ProcessIndexes: Iterators must point to int.");
    auto a = start;
    size_t size = 0;

    for (; a != end; ++a) {
        ++size;
    }

    Array ret(size, base.cols());
    int j = 0;

    for (; start != end; ++start) {
        ret.row(j++) = base.row(*start);
    }

    return ret;
}

template <typename Iterator, typename Array>
inline Array booledColAccess(const Array &base, Iterator start, Iterator end)
{
    static_assert(
        std::is_same<typename std::iterator_traits<Iterator>::value_type, bool>::value,
        "ProcessIndexes: Iterators must point to bool.");
    //TODO: This won't work, correct it
    auto a = start;
    Array ret(base.rows(), count(a, end, true));
    int j = 0;

    for (size_t k = 0; start != end; ++start, ++k) {
        if (*start) {
            ret.col(j++) = base.col(k);
        }
    }

    return ret;
}

template <typename Iterator, typename Array>
inline Array indexedColAccess(const Array &base, Iterator start, Iterator end)
{
    static_assert(
        std::is_same<typename std::iterator_traits<Iterator>::value_type, int>::value,
        "ProcessIndexes: Iterators must point to int.");
    //TODO: This won't work, correct it
    auto a = start;
    size_t size = 0;

    for (; a != end; ++a) {
        ++size;
    }

    Array ret(base.rows(), size);
    int j = 0;

    for (; start != end; ++start) {
        ret.col(j++) = base.col(*start);
    }

    return ret;
}


inline std::vector<int> indexSort(Eigen::ArrayXd arr)
{
    std::vector<int> indexes;

    for (int i = 0; i < arr.size(); ++i) {
        indexes.push_back(i);
    }

    std::sort(indexes.begin(), indexes.end(), [arr](size_t a, size_t b) {
        return arr(a) < arr(b);
    });
    return indexes;
}

inline void indexFilter(Eigen::ArrayXXd X, std::function<bool(Eigen::ArrayXd)> pred,
                        std::vector<int> indexes[2])
{
    indexes[0].clear();
    indexes[1].clear();

    for (int i = 0; i < X.rows(); ++i) {
        if (pred(X.row(i))) {
            indexes[0].push_back(i);
        } else {
            indexes[1].push_back(i);
        }
    }
}

inline void indexFilter(Eigen::ArrayXd X, std::function<bool(double)> pred,
                        std::vector<int> indexes[2])
{
    indexes[0].clear();
    indexes[1].clear();

    for (int i = 0; i < X.size(); ++i) {
        if (pred(X(i))) {
            indexes[0].push_back(i);
        } else {
            indexes[1].push_back(i);
        }
    }
}

inline double std(Eigen::ArrayXd C)
{
    return (((C.rowwise() - C.colwise().mean())).pow(2).colwise().sum() /
            (C.rows() - 1)).sqrt()(0);
}

inline Eigen::ArrayXd rowwiseStd(Eigen::ArrayXXd C)
{
    return (((C.rowwise() - C.colwise().mean())).pow(2).colwise().sum() /
            (C.rows() - 1)).sqrt();
}

inline Eigen::ArrayXd colwiseStd(Eigen::ArrayXXd C)
{
    return (((C.colwise() - C.rowwise().mean())).pow(2).rowwise().sum() /
            (C.cols() - 1)).sqrt();
}

//pinv: http://eigen.tuxfamily.org/bz/show_bug.cgi?id=257#c20
template<typename _Matrix_Type_>
_Matrix_Type_ pinv(const _Matrix_Type_ &a,
                   double epsilon = std::numeric_limits<double>::epsilon())
{
    Eigen::JacobiSVD< _Matrix_Type_ > svd(a, Eigen::ComputeThinU | Eigen::ComputeThinV);
    double tolerance = epsilon * std::max(a.cols(),
                                          a.rows()) * svd.singularValues().array().abs()(0);
    return svd.matrixV() * (svd.singularValues().array().abs() > tolerance).select(
               svd.singularValues().array().inverse(),
               0).matrix().asDiagonal() * svd.matrixU().adjoint();
}

}
namespace Evaluation
{
std::vector<std::vector<int>> stratifiedKFoldSet(const int k,
                           const Eigen::ArrayXd &y);
}
}
}
