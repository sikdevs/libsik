#pragma once

#include <bov/model.h>
#include <Eigen/Dense>
#include <cluster/kmeans.h>
#include <util.h>
#include <json/json.h>

namespace SIK
{
namespace BOV
{
/// Unigram bag of visual words model. Assumes words are drawn independently with a given probability.
class Unigram : public Model, public Json::Jsonable
{
public:
    Cluster::KMeans kmeans; ///< Underlying k-means clustering (the vocabulary)

    /** Constructor.
     * @param n_words Number of words in the model.
     */
    Unigram(size_t n_words) : kmeans(n_words) {}
    Unigram() = delete;

    virtual ~Unigram() = default;

    /** Fit model to data.
     * Determines a vocabulary from given data by K-Means clustering.
     * @param X Data matrix. @see Cluster::KMeans::fit.
     * @returns Reference to *this;
     */
    Unigram &fit(const Eigen::ArrayXXd &X) override
    {
        kmeans.fit(X);
        return *this;
    }

    std::shared_ptr<Json::Object> toJson() const override;
    Unigram &fromJson(const Json::Object &o) override;

protected:
    Eigen::ArrayXd _transformSoftAssignment(const Eigen::ArrayXXd &X) const override;
    Eigen::ArrayXd _transformHardAssignment(const Eigen::ArrayXXd &X) const override;
};

}
}
