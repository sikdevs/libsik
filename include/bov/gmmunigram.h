#pragma once

#include <bov/model.h>
#include <Eigen/Dense>
#include <cluster/gmm.h>
#include <json/json.h>
#include <util.h>

namespace SIK
{
namespace BOV
{
/// Unigram bag of visual words model. Assumes words are drawn independently with a given probability.
class GMMUnigram : public Model, public Json::Jsonable
{
public:
    Cluster::GMM gmm; ///< Underlying GMM clustering (the vocabulary)

    /** Constructor.
     * @param n_words Number of words in the model.
     */
    GMMUnigram(size_t n_words) : gmm(n_words) {}
    GMMUnigram() = delete;

    virtual ~GMMUnigram() = default;

    /** Fit model to data.
     * Determines a vocabulary from given data by GMM clustering.
     * @param X Data matrix. @see Cluster::GMM::fit.
     * @returns Reference to *this;
     */
    GMMUnigram &fit(const Eigen::ArrayXXd &X) override
    {
        gmm.fit(X);
        return *this;
    }

    std::shared_ptr<Json::Object> toJson() const override;
    GMMUnigram &fromJson(const Json::Object &o) override;

protected:
    Eigen::ArrayXd _transformSoftAssignment(const Eigen::ArrayXXd &X) const override;
    Eigen::ArrayXd _transformHardAssignment(const Eigen::ArrayXXd &X) const override;
};

}
}
