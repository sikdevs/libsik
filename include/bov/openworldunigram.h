#pragma once

#include <bov/model.h>
#include <Eigen/Dense>
#include <cluster/gmm.h>
#include <util.h>
#include <json/json.h>

namespace SIK
{
namespace BOV
{
/// OpenWorldUnigram bag of visual words model. Assumes words are drawn independently with a given probability.
class OpenWorldUnigram : public Model, public Json::Jsonable
{
public:
    static const int UNKNOWN = -1;
    Cluster::GMM gmm; ///< Underlying GMM clustering (the vocabulary)
    double theta = 0; ///< threshold for rejection (estimated in fit)

    /** Constructor.
     * @param n_words Number of words in the model.
     */
    OpenWorldUnigram(size_t n_words) : gmm(n_words) {}
    OpenWorldUnigram() = delete;

    virtual ~OpenWorldUnigram() = default;

    /** Fit model to data.
     * Determines a vocabulary from given data by GMM clustering and adjusts \theta.
     * @param X Data matrix. @see Cluster::GMM::fit.
     * @param rho Fraction of low level features that should be accepted.
     * @returns Reference to *this;
     */
    OpenWorldUnigram &fit(const Eigen::ArrayXXd &X, const double rho);
    OpenWorldUnigram &fit(const Eigen::ArrayXXd &X) override {
      return fit(X, 0.85);
    }

    std::shared_ptr<Json::Object> toJson() const override;
    OpenWorldUnigram &fromJson(const Json::Object &o) override;

protected:
    Eigen::ArrayXd _transformSoftAssignment(const Eigen::ArrayXXd &X) const override;
    Eigen::ArrayXd _transformHardAssignment(const Eigen::ArrayXXd &X) const override;
};

}
}
