#pragma once

#include <string>
#include <Eigen/Dense>
#include <vector>

namespace SIK
{
namespace BOV
{
/// Mode of assignment in Model::transform()
enum class Assignment { Hard, Soft };

/// Base class for bag of visual words models.
class Model
{
public:
    virtual ~Model() = default;

    /** Fit a model to given data.
     * @param X Data matrix: Rows contains samples, cols the features.
     * @returns Reference to *this.
     */
    virtual Model &fit(const Eigen::ArrayXXd &X) = 0;

    /** Feature extraction.
     * @param X Feature matrix of an object.
     * @param mode Mode of feature assignment (Hard or Soft). May be ignored by the model if unsuitable.
     * @returns Feature descriptor of the object.
     */
    inline Eigen::ArrayXd transform(const Eigen::ArrayXXd &X,
                                    Assignment mode = Assignment::Hard) const
    {
        return Assignment::Hard == mode ? _transformHardAssignment(X)
               : _transformSoftAssignment(X);
    }

    /** Feature extraction for a list of objects.
     * @param Xs Vector of feature matrices of a list of objects.
     * @param mode Mode of feature assignment (Hard or Soft). May be ignored by the model if unsuitable.
     * @returns List of feature descriptors of the objects.
     */
    std::vector<Eigen::ArrayXd> transform(const std::vector<Eigen::ArrayXXd> &Xs,
                                          Assignment mode = Assignment::Hard) const
    {
        std::vector<Eigen::ArrayXd> out;
        out.reserve(Xs.size());

        for (auto x : Xs) {
            out.push_back(transform(x, mode));
        }

        return out;
    }

protected:
    /// To implement by sub-class: Hard assignment feature extraction.
    virtual Eigen::ArrayXd _transformHardAssignment(const Eigen::ArrayXXd &X) const = 0;
    /// To implement by sub-class: Soft assignment feature extraction.
    virtual Eigen::ArrayXd _transformSoftAssignment(const Eigen::ArrayXXd &X) const = 0;
};

}

}
