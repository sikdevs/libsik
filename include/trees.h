#pragma once

// include all tree models here
#include <trees/decisionTree.h>
#include <trees/gaussianMixtureTree.h>

// just for doxygen
namespace SIK
{
/// Tree based models.
namespace Trees {}
}