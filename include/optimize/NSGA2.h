#pragma once

#include <Eigen/Dense>
#include <vector>

namespace SIK
{
namespace Optimize
{
class NSGA2
{
public:
    /**
     * Individual for the genetic algorithm.
     * */
    struct Individual {

        /**
         * Vector of chromosomes.
         * */
        std::vector<bool> chromosome;

        /**
         * Vector of fitness.
         * */
        std::vector<double> fitness;

        /**
         * Crowding distance.
         * */
        double distance;

        /**
         * Nondomination rank.
         * */
        int rank;

        /**
         * Check which one is in the lesser crowded region.
         * */
        bool operator<(const Individual &i) const
        {
            return rank < i.rank || (rank == i.rank && distance > i.distance);
        }
    };

    Eigen::ArrayXd objectiveRange; //< Range of values for each objective.
    std::vector<Individual> population; //< Current population.
    size_t generation; //< Current generation.


    /**
     * Number of objectives to optimize (automatically determined in optimize).
     * */
    size_t objectiveCount;

    /**
     * Size of a population.
     * */
    size_t populationSize;

    /**
     * Number of generations, aka stop-criterion.
     * */
    size_t maxGeneration;

    /**
     * @param populationSize size of a population.
     * @param generations number of generations, aka stop-criterion.
     * @param assign_fitness Fitnessfunction to be used.
     * @param recombine recombination to be used.
     * @param mutate zu mutation to be used.
     * @param generator generatorfunction to be used.
     **/
    NSGA2(size_t populationSize, size_t generations,
          std::function<Individual()> generator,
          std::function<Individual(Individual&)> assign_fitness,
          std::function<Individual(Individual, Individual)> recombine,
          std::function<Individual(Individual)> mutate)
        : populationSize(populationSize)
        , maxGeneration(generations)
        , generator(generator)
        , assign_fitness(assign_fitness)
        , recombine(recombine)
        , mutate(mutate)
    {
    }
    NSGA2() = delete;

    ~NSGA2() {}

    /**
     * Sort population into pareto shells.
     * @return Vectore of pareto shells from outer to inner.
     **/
    std::vector<std::vector<Individual *>> fastNonDominantedSort();

    /**
     * Assign the crowding distance to a vector of Individuals.
     * @param[in,out] individuals Individuals to assign crowding distance to.
     * @return Reference to this.
     **/
    NSGA2 &crowdingDistanceAssignment(std::vector<Individual *> &individuals);

    /**
     * Performs the optimization after correct initialization through the constructor.
     * @param bounds_objective The bounds to be adhered.
     * @return Reference to this.
     **/
    NSGA2 &optimize(Eigen::ArrayXXd &bounds_objective);

    /**
     * Select two parents at random of the given set and choose the better ones.
     * @param parents Set of parents for the tournament selection.
     * @return Better one of the two selected parents.
     **/
    Individual tournamentSelection(std::vector<Individual> &parents);

    /**
     * Add new individuals to population by recombining existing solutions.
     * @return Reference to this.
     **/
    NSGA2 &generateOffspring();

protected:
    std::function<Individual()> generator;
    std::function<Individual(Individual&)> assign_fitness;
    std::function<Individual(Individual, Individual)> recombine;
    std::function<Individual(Individual)> mutate;
};
}
}
