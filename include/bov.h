#pragma once

// include all bov models here
#include <bov/model.h>
#include <bov/unigram.h>
#include <bov/gmmunigram.h>
#include <bov/openworldunigram.h>

// just for doxygen
namespace SIK
{
/// Bag of Visual Words models.
namespace BOV {}
}