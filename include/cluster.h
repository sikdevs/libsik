#pragma once

// include all clustering methods here
#include <cluster/base.h>
#include <cluster/kmeans.h>
#include <cluster/gmm.h>

// just for doxygen
namespace SIK
{
/// Cluster analysis.
namespace Cluster {}
}