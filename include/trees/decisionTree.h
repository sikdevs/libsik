#pragma once

#include <Eigen/Dense>
#include <trees/tree.h>
#include <json/json.h>
#include <vector>
#include <map>
#include <memory>
#include <type_traits>

namespace SIK
{
namespace Trees
{

/// Type of split used in decision trees
struct Split : public Json::Jsonable {
    int featureIndex = -1; //< Index of the feature to split on.
    double threshold = 0; //< Threshold on the feature that induces the split.
    Eigen::ArrayXd
    classDistribution; //< Expected distributions of classes at this split.

    /**
     * Check if sample should be rejected, i.e., if it cannot be classified.
     * @param x Sample to test.
     * @returns true, if the sample should be rejected, false otherwise.
     */
    inline bool reject(const Eigen::ArrayXd &x)
    {
        return false;
    }

    virtual ~Split() {}
    virtual std::shared_ptr<Json::Object> toJson() const override;
    virtual Split &fromJson(const Json::Object &o) override;
};

/// Basic split on one feature with no rejection.
struct BasicSplit : public Split {
    virtual ~BasicSplit() {}
    virtual std::shared_ptr<Json::Object> toJson() const override;
    virtual BasicSplit &fromJson(const Json::Object &o) override;
};

/// Split with upper and lower rejection bound on the feature.
struct MinmaxRejectionSplit : public Split {
    double upper; //< Reject if feature value is larger than this bound.
    double lower; //< Reject if feature value is smaller than this bound.

    inline bool reject(const Eigen::ArrayXd &x)
    {
        return x[featureIndex] < lower || x[featureIndex] > upper;
    }

    virtual ~MinmaxRejectionSplit() {}
    virtual std::shared_ptr<Json::Object> toJson() const override;
    virtual MinmaxRejectionSplit &fromJson(const Json::Object &o) override;
};

/// Split with assumption of normally distributed features. Rejects if the likelihood of seeing a given feature is small.
struct GaussianRejectionSplit : public Split {
    double mean; //< mean of the Gaussian
    double std; //< standard deviation of the Gaussian

    inline bool reject(const Eigen::ArrayXd &x)
    {
        return std::abs(x[featureIndex] - mean) >= std * 2; // TODO: Parametrize!
    }

    virtual ~GaussianRejectionSplit() {}
    virtual std::shared_ptr<Json::Object> toJson() const override;
    virtual GaussianRejectionSplit &fromJson(const Json::Object &o) override;
};

/// Split with assumption of normally distributed features. Rejects if the likelihood of seeing a given *sample* is small.
struct MahalanobisRejectionSplit : public Split {
    Eigen::VectorXd mean; //< Center of the Gaussian.
    Eigen::MatrixXd precision; //< Inverse covariance matrix of the Gaussian.
    double rejectionThreshold; //< Rejection threshold.

    inline bool reject(const Eigen::ArrayXd &x)
    {
        return Util::Distance::sqmahalanobis(x, mean, precision) > rejectionThreshold;
    }

    virtual ~MahalanobisRejectionSplit() {}
    virtual std::shared_ptr<Json::Object> toJson() const override;
    virtual MahalanobisRejectionSplit &fromJson(const Json::Object &o) override;
};

/** A decision tree with optional reject option.
 * @see BasicSplit
 * @see MinmaxRejectionSplit
 * @see GaussianRejectionSplit
 * @see MahalanobisRejectionSplit
 */
template<typename SplitType>
class DecisionTree : public Json::Jsonable
{
public:
    std::shared_ptr<Node<SplitType>> root; //< Root node of the tree

    size_t max_depth; //< Stopping criterion in learning: maximum depth of the tree.
    double min_impurity_gain; //< Stopping criterion in learning: minimal utility of a new split.
    size_t N_min; //< Stopping criterion in learning: minimal nuber of samples.
    std::vector<int> labels; //< Class labels.

    /** Constructor
     * @param max_depth Stopping criterion in learning: maximum depth of the tree.
     * @param min_impurity_gain Stopping criterion in learning: minimal utility of a new split.
     * @param N_min Stopping criterion in learning: minimal nuber of samples.
     */
    DecisionTree(size_t max_depth, double min_impurity_gain, size_t N_min)
        : max_depth(max_depth)
        , min_impurity_gain(min_impurity_gain)
        , N_min(N_min)
    {}
    virtual ~DecisionTree() {}

    /** Learn a decision tree from data
     * @param X Data matrix where each row is a sample.
     * @param y Class labels for each sample.
     * @returns Reference to *this.
    */
    DecisionTree<SplitType> &fit(const Eigen::ArrayXXd &X, const Eigen::ArrayXi &y);

    /** Simplify the decision tree with minimal loss of classification accuracy.
     * @param X Data matrix where each row is a sample.
     * @param y Class labels for each sample.
     * @returns Reference to *this.
    */
    DecisionTree<SplitType> &prune(const Eigen::ArrayXXd &X, const Eigen::ArrayXi &y);

    /** Predict the class label of a sample.
     * @param x Sample to classify.
     * @returns Predicted class label.
    */
    int predict(const Eigen::ArrayXd &x) const;

    /** Predict likelihood of each class.
     * @param x Sample to classify.
     * @returns Predicted likelihoods or vector with -1 as single entry if x was rejected.
    */
    Eigen::ArrayXd transform(const Eigen::ArrayXd &x) const;

    virtual std::shared_ptr<Json::Object> toJson() const override;
    virtual DecisionTree<SplitType> &fromJson(const Json::Object &o) override;
};

// template specialization in decisionTree.cpp
template<typename T>
DecisionTree<T> &DecisionTree<T>::fit(const Eigen::ArrayXXd &,
                                      const Eigen::ArrayXi &)
{
    //TODO: not sure if this is the correct way
    //static_assert(sizeof(T)==0, "fit() not implemented for given Split type");
    return *this;
}

template<>
DecisionTree<BasicSplit> &DecisionTree<BasicSplit>::fit(const Eigen::ArrayXXd &,
        const Eigen::ArrayXi &);

template<>
DecisionTree<MinmaxRejectionSplit> &DecisionTree<MinmaxRejectionSplit>::fit(
    const Eigen::ArrayXXd &, const Eigen::ArrayXi &);

template<>
DecisionTree<GaussianRejectionSplit> &DecisionTree<GaussianRejectionSplit>::fit(
    const Eigen::ArrayXXd &, const Eigen::ArrayXi &);

template<>
DecisionTree<MahalanobisRejectionSplit>
&DecisionTree<MahalanobisRejectionSplit>::fit(
    const Eigen::ArrayXXd &, const Eigen::ArrayXi &);

template <typename T>
DecisionTree<T> &DecisionTree<T>::prune(const Eigen::ArrayXXd &,
                                        const Eigen::ArrayXi &)
{
    //static_assert(sizeof(T)==0, "prune() not implemented for given Split type");
    return *this;
}

/*
template<>
DecisionTree<BasicSplit> &DecisionTree<BasicSplit>::prune(const Eigen::ArrayXXd &,
        const Eigen::ArrayXi &);

template<>
DecisionTree<MinmaxRejectionSplit> &DecisionTree<MinmaxRejectionSplit>::prune(
    const Eigen::ArrayXXd &, const Eigen::ArrayXi &);

template<>
DecisionTree<GaussianRejectionSplit> &DecisionTree<GaussianRejectionSplit>::prune(
    const Eigen::ArrayXXd &, const Eigen::ArrayXi &);

template<>
DecisionTree<MahalanobisRejectionSplit>
&DecisionTree<MahalanobisRejectionSplit>::prune(
    const Eigen::ArrayXXd &, const Eigen::ArrayXi &);
*/

template<typename T>
Eigen::ArrayXd DecisionTree<T>::transform(const Eigen::ArrayXd &X) const
{
    //static_assert(sizeof(T)==0, "transform() not implemented for given Split type");
    return X;
}

template<>
Eigen::ArrayXd DecisionTree<BasicSplit>::transform(const Eigen::ArrayXd &) const;

template<>
Eigen::ArrayXd DecisionTree<MinmaxRejectionSplit>::transform(
    const Eigen::ArrayXd &) const;

template<>
Eigen::ArrayXd DecisionTree<GaussianRejectionSplit>::transform(
    const Eigen::ArrayXd &) const;

template<>
Eigen::ArrayXd DecisionTree<MahalanobisRejectionSplit>::transform(
    const Eigen::ArrayXd &) const;

template<typename T>
int DecisionTree<T>::predict(const Eigen::ArrayXd &x) const
{
    auto p = transform(x);

    // sample was rejected
    if (p[0] == -1) {
        return -1;
    }

    // get class label
    int index;
    p.maxCoeff(&index);
    return labels[index];
}


template<typename T>
std::shared_ptr<Json::Object> DecisionTree<T>::toJson() const
{
    auto obj = std::make_shared<Json::Object>();
    obj->put("type", "DecisionTree");
    obj->put("max_depth", max_depth);
    obj->put("min_impurity_gain", min_impurity_gain);
    obj->put("N_min", N_min);
    obj->put("root", root->toJson());
    return obj;
}

template<typename T>
DecisionTree<T> &DecisionTree<T>::fromJson(const Json::Object &o)
{
    auto s = (Json::String *)o.get("type").get();

    if (s->data != "DecisionTree") {
        throw std::runtime_error("Invalid object type. Expected DecisionTree, got " +
                                 s->data);
    }

    max_depth = size_t(((Json::Number *) o.get("max_depth").get())->data);
    min_impurity_gain = ((Json::Number *) o.get("min_impurity_gain").get())->data;
    N_min = int(((Json::Number *) o.get("N_min").get())->data);
    root = std::make_shared<Node<T>>(0);
    root->fromJson(*((Json::Object *) o.get("root").get()));
    return *this;
}

}
}
