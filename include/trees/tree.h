#pragma once

#include <Eigen/Dense>
#include <memory>
#include <string>
#include <sstream>
#include <vector>
#include <json/json.h>
namespace SIK
{
namespace Trees
{

template<class T> class Node : public Json::Jsonable
{
public:
    using NodePtr = std::shared_ptr<Node<T>>;

    /**
     * Parent of this node.
     * */
    NodePtr parent;

    /**
     * Children of this node.
     * */
    std::vector<NodePtr> child;

    /**
     * Depth of this node.
     * */
    size_t depth;

    /**
     * Template class Data of this Node.
     * */
    T data;

    /**
     * Constuctor, setting only the depth.
     * */
    Node(std::size_t depth)
        : parent(nullptr)
        , depth(depth)
    {}

    /**
     * Constructor, setting data an depth.
     * @param data of the node.
     * @param depth of the node.
     * */
    Node(const T &data, std::size_t depth)
        : parent(nullptr)
        , data(data)
        , depth(depth)
    {}
    Node() = delete;;

    /**
     * True, if this is a leaf, otherwise false.
     * */
    bool isLeaf() const
    {
        return child.empty();
    }

    std::shared_ptr<Json::Object> toJson() const override
    {
        std::shared_ptr<Json::Object> json = std::make_shared<Json::Object>();
        json->put("type", std::string("Node"));
        json->put("depth", (int)(depth));
        std::shared_ptr<Json::Object> d = data.toJson();
        json->put("data", d);

        if (!isLeaf()) {
            std::shared_ptr<Json::Array> arr = std::make_shared<Json::Array>();

            for (const auto &c : child) {
                arr->push_back(c->toJson());
            }

            json->put("child", arr);
        }

        return json;
    }

    Node &fromJson(const Json::Object &o) override
    {
        auto type = (Json::String *) o.get("type").get();
        assert(type->data == std::string("Node"));

        auto de = ((Json::Number *) o.get("depth").get());
        depth = size_t(de->data);
        auto d = ((Json::Object *) o.get("data").get());
        data = data.fromJson(*d);

        if (!isLeaf()) {
            auto a = ((Json::Array *) o.get("child").get());

            for (std::shared_ptr<Json::JsonBase> e : a->data) {
                auto n = std::make_shared<Node<T>>(0);
                // pointers are fun!
                auto o = (Json::Object *) e.get();
                n->fromJson(*o);
                // TODO n->parent = ???
                child.push_back(n);
            }
        }

        return *this;
    }

    /**
     * @TODO implement
     * */
    virtual ~Node() {}
};
}
}
