#pragma once

#include <Eigen/Dense>
#include <util.h>
#include <vector>
#include <map>
#include <memory>
#include <string>
#include <sstream>
#include <iostream>

namespace SIK
{
/// JSON serialization helpers.
namespace Json
{
/**
 * Base JSON Class.
 * */
class JsonBase
{
public:

    virtual std::string str() = 0;
    //virtual ~JsonBase(){};
};

/**
 * Representation of the null object in JSON.
 * */
class Null : public JsonBase
{
public:
    Null() {}
    //~Null() override{}
    std::string str() override;
    /**
     * Parses the given stringstream and returns a shared_ptr to that created Object.
     * @param ss stringstream that should be parsed
     * @returns shared_ptr to a new Json::Null object.
     * @throws runtime_error
     * */
    static std::shared_ptr<Null> parse(std::stringstream &ss);
};

/**
 * Representation of a number in JSON.
 * */
class Number : public JsonBase
{
public:
    /**
     * Holds the data as a double.
     **/
    double data;

    /**
     * Constructor, setting the value of member data.
     * @param data value of member data.
     **/
    Number(double data): data(data) {}
    //~Number() override{}
    std::string str() override;

    /**
     * Parses the given stringstream and returns a shared_ptr to that created Object.
     * @param ss stringstream that should be parsed
     * @returns shared_ptr to a new Json::Number object.
     * @throws runtime_error
     * */
    static std::shared_ptr<Number> parse(std::stringstream &ss);
};
/**
 * Representation of a bool in JSON.
 * */
class Bool : public JsonBase
{
public:
    /**
     * Holds the data as a bool.
     **/
    bool data;

    /**
     * Constructor, setting the value of member data.
     * @param data value of member data.
     **/
    Bool(bool data): data(data) {}
    //
    //~Bool() override{}
    std::string str() override;

    /**
     * Parses the given stringstream and returns a shared_ptr to that created Object.
     * @param ss stringstream that should be parsed
     * @returns shared_ptr to a new Json::Number object.
     * @throws runtime_error
     * */
    static std::shared_ptr<Bool> parse(std::stringstream &ss);
};
/**
 * Representation of a string in JSON.
 * WARNING: cannot handle " quotes.
 * */
class String : public JsonBase
{
public:
    /**
     * Holds the data as a std::string.
     **/
    std::string data;

    /**
     * Constructor, setting the value of member data.
     * @param data value of member data.
     **/
    String(std::string data): data(data) {}

    //~String() override{}
    std::string str() override;

    /**
     * Parses the given stringstream and returns a shared_ptr to that created Object.
     * @param ss stringstream that should be parsed
     * @returns shared_ptr to a new Json::String object.
     * @throws runtime_error
     * */
    static std::shared_ptr<String> parse(std::stringstream &ss);
};
/**
 * Representation of a JSON object.
 * */
class Object : public JsonBase
{
public:
    /**
     * Holds the data as a std::map<string, shared_ptr<JsonBase>.
     **/
    std::map<std::string, std::shared_ptr<JsonBase>> data;
    /**
     * Constructor with an empty data map.
     **/
    Object() {}
    //~Object() override {
    //  //for(auto it = data.begin(); it != data.end(); it++) {
    //  //  delete it->second;
    //  //}
    //}

    /**
     * Maps the given key to a Json::Null object.
     * @param name key in the map.
     * */
    Object *put(std::string name);

    /**
     * Maps the given key to a Json::Number object.
     * @param name key in the map.
     * @param obj value in the map.
     * */
    Object *put(std::string name, const double obj);

    /**
     * Maps the given key to a Json::Number object.
     * @param name key in the map.
     * @param obj value in the map.
     * */
    Object *put(std::string name, const int obj)
    {
        return put(name, (double) obj);
    }

    /**
     * Maps the given key to a Json::Number object.
     * @param name key in the map.
     * @param obj value in the map.
     * */
    Object *put(std::string name, const float obj)
    {
        return put(name, (double) obj);
    }

    /**
     * Maps the given key to a Json::Number object.
     * @param name key in the map.
     * @param obj value in the map.
     * */
    Object *put(std::string name, const size_t obj)
    {
        return put(name, (double) obj);
    }

    /**
     * Maps the given key to a Json::Bool object.
     * @param name key in the map.
     * @param obj value in the map.
     * */
    Object *put(std::string name, const bool obj);

    /**
     * Maps the given key to a Json::String object.
     * WARNING: check, that obj is of type std::string,
     * otherwise it could be mapped as a bool.
     *
     * @param name key in the map.
     * @param obj value in the map.
     * */
    Object *put(std::string name, const std::string obj);

    /**
     * Maps the given key to a shared_ptr<Json::Base>.
     * @param name key in the map.
     * @param obj value in the map.
     * */
    Object *put(std::string name, const std::shared_ptr<JsonBase> &obj);

    /**
     * Returns the value inside data with the given key.
     * @param name key in the map.
     * @returns shared_ptr<JsonBase> of the mapped object.
     * @throws runtime_error when the given key is not mapped.
     * */
    std::shared_ptr<JsonBase> get(std::string name) const;

    /**
     * TODO
     * */
    static void parsePair(std::stringstream &ss, std::shared_ptr<Object> o);

    /**
     * Parses the given stringstream and returns a shared_ptr to that created Object.
     * @param ss stringstream that should be parsed
     * @returns shared_ptr to a new Json::Object.
     * @throws runtime_error
     * */
    static std::shared_ptr<Object> parse(std::stringstream &ss);

    /**
     * Parses the given string and returns a shared_ptr to that created Object.
     * @param ss stringstream that should be parsed
     * @returns shared_ptr to a new Json::Object.
     * @throws runtime_error
     * */
    static std::shared_ptr<Object> parse(const std::string &s);
    std::string str() override;
};

/**
 * Representation of a JSON Array.
 * */
class Array : public JsonBase
{
public:
    /**
     * Holds the data as a vector.
     **/
    std::vector<std::shared_ptr<JsonBase>> data;
    //~Array() override {
    //  //for(size_t d = 0; d < data.size(); ++d){
    //  //  delete data[d];
    //  //}
    //}

    /**
     * Constructor, taking an Eigen::ArrayXXd and saves its elements rowwise as
     * JSON::Array elements with JSON::Number elements.
     * @param arr Eigen::Array, to be added.
     * */
    Array(const Eigen::ArrayXXd &arr)
    {
        for (int i = 0; i < arr.rows(); ++i) {
            std::shared_ptr<std::vector<std::shared_ptr<JsonBase>>> row(
                new std::vector<std::shared_ptr<JsonBase>>());

            for (int j = 0; j < arr.cols(); ++j) {
                row->emplace_back(std::shared_ptr<Number>(new Number(arr(i, j))));
            }

            std::shared_ptr<JsonBase> r(new Array(*row));
            push_back(r);
        }
    }

    /**
     * Constructor, taking a vector of shared_ptr<JsonBase> elements.
     * @param arr elements, to be added.
     * */
    Array(const std::vector<std::shared_ptr<JsonBase>> &arr)
    {
        for (size_t a = 0; a < arr.size(); ++a) {
            push_back(arr[a]);
        }
    }

    /**
     * Constructor, taking a vector of int values and saves them as JSON::Number elements.
     * @param arr vector of int, to be added.
     * */
    Array(const std::vector<int> arr)
    {
        for (int d : arr) {
            push_back(d);
        }
    }

    /**
     * Constructor, taking a vector of double values and saves them as JSON::Number elements.
     * @param arr vector of double, to be added.
     * */
    Array(const std::vector<double> arr)
    {
        for (double d : arr) {
            push_back(d);
        }
    }

    /**
     * Constructor, taking a vector of std::string values and saves them as JSON::String elements.
     * @param arr vector of std::string, to be added.
     * */
    Array(const std::vector<std::string> arr)
    {
        for (std::string d : arr) {
            push_back(d);
        }
    }

    /**
     * Constructor, taking a vector of bool values and saves them as JSON::Bool elements.
     * @param arr vector of bool, to be added.
     * */
    Array(const std::vector<bool> arr)
    {
        for (bool d : arr) {
            push_back(d);
        }
    }

    /**
     * Constructor, taking a vector of Eigen::VectorXd values and saves them as JSON::Array
     * elements containing their values as Eigen::Number elements.
     * @param arr vector of Eigen::VectorXd, to be added.
     * */
    Array(const std::vector<Eigen::VectorXd> &arr)
    {
        for (auto a : arr) {
            data.push_back(std::shared_ptr<Array>(new Array(a)));
        }
    }

    /**
     * Constructor, taking a vector of Eigen::MatrixXd values and saves them as JSON::Array
     * elements containing their values as Json::Array elements with Eigen::Number elements.
     * @param arr vector of Eigen::MatrixXd, to be added.
     * */
    Array(const std::vector<Eigen::MatrixXd> &arr)
    {
        for (auto a : arr) {
            data.push_back(std::shared_ptr<Array>(new Array(a)));
        }
    }

    /**
     * Constructor, taking an Eigen::ArrayXXd and saves its elements rowwise as
     * JSON::Array elements with JSON::Number elements.
     * @param arr Eigen::ArrayXXd, to be parsed.
     * */ Array(const std::vector<Eigen::ArrayXXd> &arr)
    {
        for (auto a : arr) {
            data.push_back(std::shared_ptr<Array>(new Array(a)));
        }
    }

    /**
     * Constructor, with empty data.
     * */
    Array() {}

    /**
     * Creates a new Json::Number element and pushes it at the end of the data vector.
     * @param obj value of the element to be pushed
     * */
    void push_back(const int obj);

    /**
     * Creates a new Json::Number element and pushes it at the end of the data vector.
     * @param obj value of the element to be pushed
     * */
    void push_back(const double obj);

    /**
     * Creates a new Json::Bool element and pushes it at the end of the data vector.
     * @param obj value of the element to be pushed
     * */
    void push_back(const bool obj);

    /**
     * Creates a new Json::String element and pushes it at the end of the data vector.
     * @param obj value of the element to be pushed
     * */
    void push_back(const std::string obj);

    /**
     * Pushes obj at the end of the data vector.
     * @param obj value of the element to be pushed
     * */
    void push_back(const std::shared_ptr<JsonBase> &obj);

    /**
     * Creates a new Eigen::ArrayXXd with the values in data.
     * @throws runtime_error if data contains a non-JSON::Number
     * */
    Eigen::ArrayXXd eigenArrayXXd();

    /**
     * Creates a new Eigen::ArrayXd with the values in data.
     * @throws runtime_error if data contains a non-JSON::Number
     * */
    Eigen::ArrayXd eigenArrayXd();
    std::string str() override;

    /**
     * Parses the given string and returns a shared_ptr to that created Object.
     * @param ss stringstream that should be parsed
     * @returns shared_ptr to a new Json::Array.
     * @throws runtime_error
     * */
    static std::shared_ptr<Array> parse(std::stringstream &ss);
};

struct Jsonable : public Util::Serializable {

    /**
     * Returns a shared_ptr to a Json::Object representing the serialized Jsonable.
     * @return shared_ptr<Json::Object> to the serialized Jsonable.
     * */
    virtual std::shared_ptr<Json::Object> toJson() const = 0;

    /**
     * Deserializes the Jsonable with the given Json::Object o.
     * @param o Json::Object to be deserialized.
     * */
    virtual Jsonable &fromJson(const Json::Object &o) = 0;
    virtual ~Jsonable() {}

    std::string serialize() const override
    {
        std::shared_ptr<Json::Object> o = toJson();
        std::string s = o->str();
        return s;
    }
    Jsonable &deserialize(const std::string &s) override
    {
        std::shared_ptr<Json::Object> o = Json::Object::parse(s);
        return fromJson(*o);
    }
};
}
}
