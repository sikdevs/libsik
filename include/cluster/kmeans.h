#pragma once

#include <cluster/base.h>
#include <Eigen/Dense>
#include <util.h>
#include <json/json.h>

namespace SIK
{
namespace Cluster
{
/// K-Means clustering.
class KMeans : public Cluster::Base, public Json::Jsonable
{
public:
    size_t K; ///< Number of clusters.
    size_t D; ///< Dimension of the feature space.
    std::vector<Eigen::VectorXd> means; ///< Cluster centers.

    /** Constructor.
     * @param K Number of clusters.
     */
    KMeans(size_t K) : K(K), D(0) {}
    KMeans() = delete;

    KMeans &fit(const Eigen::ArrayXXd &X) override
    {
        return fit(X, 100);
    }

    /** Fit K-Means model to data.
     * Implements the iterative Lloyd algorithm with K-Means++ initialization.
     * @param X Data matrix. @see Cluster::Base::fit.
     * @param n_iter Maximum number of iterations.
     * @param eps Termination threshold on change of cluster centers between iterations.
     * @returns Reference to *this;
     */
    inline KMeans &fit(const Eigen::ArrayXXd &X, size_t n_iter, double eps = 1e-10)
    {
        return intializeCenters(X).findClustering(X, n_iter, eps);
    }

    /** Initialize cluster centers with K-Means++.
     * @param X Data matrix. @see Cluster::Base::fit.
     * @returns Reference to *this;
     */
    KMeans &intializeCenters(const Eigen::ArrayXXd &X);

    /** Fit K-Means model to data.
     * Implements the iterative Lloyd algorithm. means must be initialized.
     * Allows to re-fit existing clustering to new data or to fit data with a custom initial guess.
     * @param X Data matrix. @see Cluster::Base::fit.
     * @param n_iter Maximum number of iterations.
     * @param eps Termination threshold on change of cluster centers between iterations.
     * @returns Reference to *this;
     */
    KMeans &findClustering(const Eigen::ArrayXXd &X, size_t n_iter, double eps);

    int predict(const Eigen::ArrayXd &x) const override;
    Eigen::ArrayXd transform(const Eigen::ArrayXd &x) const override;

    std::shared_ptr<Json::Object> toJson() const override;
    KMeans &fromJson(const Json::Object &o) override;
};
}
}
