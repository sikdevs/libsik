#pragma once

#include <Eigen/Dense>
#include <vector>
#include <util.h>

namespace SIK
{
namespace Cluster
{
/// Base class for all other clustering methods
class Base
{
public:
    virtual ~Base() {}

    /** Fit model to data.
     * @param X Data to fit model as 2D-Array. Rows are samples, columns contain the variables.
     * @returns Reference to *this.
     */
    virtual Base &fit(const Eigen::ArrayXXd &X) = 0;

    /** Get cluster membership.
     * Determines the id of the cluster the vector X falls into
     * @param x Vector / 1D-Array to query.
     * @returns Id of the cluster X falls into.
     */
    virtual int predict(const Eigen::ArrayXd &x) const = 0;

    /** Get degree of memebership of each cluster.
     * Determines degree of membership (the similarity) for each cluster in the model.
     * Can also be dis-similarity instead of similarity, e.g. in K-Means.
     * @param x Vector / 1D-Array to query.
     * @returns Vector / 1D-Array of cluster memberships degrees.
     */
    virtual Eigen::ArrayXd transform(const Eigen::ArrayXd &x) const = 0;

    /** Convenience wrapper to fit to a list of 1D-Arrays.
     * @param Xs vector of 1D-Arrays.
     * @returns Reference to *this;
     */
    inline Base &fit(const std::vector<Eigen::ArrayXd> &Xs)
    {
        return fit(Util::concatRows(Xs));
    }

    /** Convenience wrapper to predict a list of 1D-Arrays.
     * @param Xs vector of 1D-Arrays.
     * @returns Array of cluster ids.
     */
    virtual Eigen::ArrayXi predict(const std::vector<Eigen::ArrayXd> &Xs) const
    {
        Eigen::ArrayXi out = Eigen::ArrayXi::Zero(Xs.size());
        size_t i = 0;

        for (const auto &x : Xs) {
            out(i) = predict(x);
            i++;
        }

        return out;
    }

    /** Convenience wrapper to transform a list of 1D-Arrays..
     * @param Xs vector of 1D-Arrays.
     * @returns 2D-Array of cluster memberships. Each row corresponds to an item in Xs.
     */
    virtual Eigen::ArrayXXd transform(const std::vector<Eigen::ArrayXd> &Xs) const
    {
        // transform first item to determine required output size
        auto xt = transform(Xs.at(0));
        const int D_out = xt.size();
        Eigen::ArrayXXd out(Xs.size(), D_out);
        out.row(0) = xt;

        // now to the rest
        for (size_t i = Xs.size(); i > 0; --i) { // remember: we did item 0 already!
            out.row(i) = transform(Xs.at(i));
        }

        return out;
    }

};
}
}
