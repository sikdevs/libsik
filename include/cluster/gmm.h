#pragma once

#include <cluster/base.h>
#include <Eigen/Dense>
#include <util.h>
#include <json/json.h>

namespace SIK
{
namespace Cluster
{

/// Gaussian Mixture Model clustering.
class GMM : public Cluster::Base, public Json::Jsonable
{
public:
    size_t K; ///< Number of cluster centers.
    size_t D; ///< Dimension of the feature space.
    std::vector<Eigen::VectorXd> means; ///< Cluster centers / Means of the components.
    std::vector<Eigen::MatrixXd> covariances; ///< Covariance matrices of the components.
    std::vector<Eigen::MatrixXd> precisions; ///< Precision matrices of the components.
    std::vector<double> det_covs; ///< Determinants of the covariance matrices.
    std::vector<double> priors; ///< Priors on cluster memberships.

    /** Constructor.
     * @param K Number of clusters to estimate.
     */
    GMM(size_t K) : K(K), D(0) {}
    GMM() = delete;

    GMM &fit(const Eigen::ArrayXXd &X) override
    {
        return fit(X, 100);
    }

    /** Fit GMM to data.
     * @param X Data matrix. @see Cluster::Base::fit.
     * @param n_iter Maximum number of iterations.
     * @param eps Termination threshold on change of log-likelihood between iterations.
     * @returns Reference to *this;
     */
    GMM &fit(const Eigen::ArrayXXd &X, size_t n_iter, double eps = 1e-10);

    int predict(const Eigen::ArrayXd &X) const override;
    Eigen::ArrayXd transform(const Eigen::ArrayXd &x) const override;

    std::shared_ptr<Json::Object> toJson() const override;
    GMM &fromJson(const Json::Object &o) override;
};
}
}
