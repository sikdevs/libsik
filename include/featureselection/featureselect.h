#pragma once

#include <Eigen/Dense>
#include <vector>

namespace SIK
{
namespace Featureselection
{
class CostSensitive
{
public:
    struct Selection {
        std::vector<int> featureIndices;
        double merit;
        double meanCost;
        double costScatter;
    };
    /**
     * @param X Features
     * @param y Classes
     * @param C Costs
     * @param numCandidates Number of selection candidates to generate
     * @param iterations Number of iterations in the selection algorithm
     * @return vector of selection candidates
     */
    static std::vector<Selection> select(Eigen::ArrayXXd &X, Eigen::ArrayXi &y,
                                         Eigen::ArrayXXd &C, size_t numCandidates = 40, size_t iterations = 100);
};
}
}
