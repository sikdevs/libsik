#pragma once

#include <optimize/NSGA2.h>
#include <util.h>
#include <Eigen/Dense>
#include <vector>

namespace SIK
{
namespace Featureselection
{
namespace Multicriteria
{
double multicorr(const Eigen::ArrayXXd &X, const Eigen::ArrayXi &y);
double LDA_score(const Eigen::ArrayXXd &X, const Eigen::ArrayXi &y);
}
}
}
